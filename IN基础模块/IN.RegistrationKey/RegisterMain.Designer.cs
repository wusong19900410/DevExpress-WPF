﻿
namespace Newu.RegistrationKey
{
    partial class RegisterMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterMain));
            this.btnRegist = new DevExpress.XtraEditors.SimpleButton();
            this.lbRegistInfo = new DevExpress.XtraEditors.LabelControl();
            this.SuspendLayout();
            // 
            // btnRegist
            // 
            this.btnRegist.Location = new System.Drawing.Point(167, 133);
            this.btnRegist.Name = "btnRegist";
            this.btnRegist.Size = new System.Drawing.Size(75, 32);
            this.btnRegist.TabIndex = 3;
            this.btnRegist.Text = "导入授权码";
            this.btnRegist.Click += new System.EventHandler(this.btnRegist_Click);
            // 
            // lbRegistInfo
            // 
            this.lbRegistInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbRegistInfo.Appearance.ForeColor = System.Drawing.Color.DarkGreen;
            this.lbRegistInfo.Appearance.Options.UseForeColor = true;
            this.lbRegistInfo.Appearance.Options.UseTextOptions = true;
            this.lbRegistInfo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbRegistInfo.AppearancePressed.Options.UseTextOptions = true;
            this.lbRegistInfo.AppearancePressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbRegistInfo.AppearancePressed.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbRegistInfo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbRegistInfo.Location = new System.Drawing.Point(19, 37);
            this.lbRegistInfo.Name = "lbRegistInfo";
            this.lbRegistInfo.Size = new System.Drawing.Size(376, 43);
            this.lbRegistInfo.TabIndex = 2;
            this.lbRegistInfo.Text = "Info";
            // 
            // RegisterMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 203);
            this.Controls.Add(this.btnRegist);
            this.Controls.Add(this.lbRegistInfo);
            this.IconOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("RegisterMain.IconOptions.SvgImage")));
            this.Name = "RegisterMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "授权管理";
            this.Load += new System.EventHandler(this.RegisterMain_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnRegist;
        private DevExpress.XtraEditors.LabelControl lbRegistInfo;
    }
}