﻿using DevExpress.XtraEditors;
using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Newu.RegistrationKey
{
    public partial class RegisterMain : XtraForm
    {
        private string encryptComputer = string.Empty;
        private bool isRegist = false;
        private const int timeCount = 60;
        public RegisterMain()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;


            //设置皮肤
            DevExpress.UserSkins.BonusSkins.Register();
            DevExpress.UserSkins.OfficeSkins.Register();
            DevExpress.Skins.SkinManager.EnableFormSkins();
            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            this.LookAndFeel.UseDefaultLookAndFeel =true;
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = "Darkroom";
        }

        private void RegisterMain_Load(object sender, EventArgs e)
        {
            string computer = ComputerInfo.GetComputerInfo();
            encryptComputer = new EncryptionHelper().EncryptString(computer);
            if (CheckRegist() == true)
            {
                lbRegistInfo.Text = "已注册";
            }
            else
            {
                lbRegistInfo.Text = "待注册，运行一分钟后自动关闭";
                RegistFileHelper.WriteComputerInfoFile(encryptComputer);
                TryRunForm();
            }
        }
        ///   
            /// 试运行窗口  
            ///   
        private void TryRunForm()
        {
            Thread threadClose = new Thread(CloseForm);
            threadClose.IsBackground = true;
            threadClose.Start();
        }
        private bool CheckRegist()
        {
            EncryptionHelper helper = new EncryptionHelper();
            string md5key = helper.GetMD5String(encryptComputer);
            return CheckRegistData(md5key);
        }
        private bool CheckRegistData(string key)
        {
            if (RegistFileHelper.ExistRegistInfofile() == false)
            {
                isRegist = false;
                return false;
            }
            else
            {
                string info = RegistFileHelper.ReadRegistFile();
                var helper = new EncryptionHelper(EncryptionKeyEnum.KeyB);
                string registData = helper.DecryptString(info);
                if (key == registData)
                {
                    isRegist = true;
                    return true;
                }
                else
                {
                    isRegist = false;
                    return false;
                }
            }
        }
        private void CloseForm()
        {
            int count = 0;
            while (count < timeCount && isRegist == false)
            {
                if (isRegist == true)
                {
                    return;
                }
                Thread.Sleep(1 * 1000);
                count++;
            }
            if (isRegist == true)
            {
                return;
            }
            else
            {
                this.Close();
            }
        }

        private void btnRegist_Click(object sender, EventArgs e)
        {
            if (lbRegistInfo.Text == "已注册")
            {
                MessageBox.Show("已经注册～");
                return;
            }
            string fileName = string.Empty;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileName = openFileDialog.FileName;
            }
            else
            {
                return;
            }
            string localFileName = string.Concat(
            Environment.CurrentDirectory,
            Path.DirectorySeparatorChar,
            RegistFileHelper.RegistInfofile);
            if (fileName != localFileName)
                File.Copy(fileName, localFileName, true);

            if (CheckRegist() == true)
            {
                lbRegistInfo.Text = "已注册";
                MessageBox.Show("注册成功～");
            }
        }
    }
}