﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace IN.DataBase
{
    /// <summary>
    /// 所有BLL的基类 对应BaseService，调用同时把日志记录, 提高开发效率
    /// </summary>
    public class BaseBLL<T> : IBaseIBLL<T> where T : class
    {
        private readonly NLog.Logger Logger = NLog.LogManager.GetLogger("BaseBLL");
        public readonly IBaseService<T> iBaseService = new BaseService<T>();

        public IDbConnection DbConnection { get { return ((BaseService<T>)iBaseService).Connection; } }

        /// <summary>
        /// 返回数据库所有的对象List集合
        /// </summary>
        /// <returns></returns>
        public List<T> GetListAll()
        {
            Logger.Info("used GetListAll()");
            List<T> list = null;
            try
            {
                list = iBaseService.GetAll().ToList();
            }
            catch(Exception ex)
            {//GetAll<T> only supports an entity with a [Key] or an [ExplicitKey] property
                Logger.Error(ex, ex.Message);
                MessageBox.Show(ex.Message);
            }
            return list;
        }
        /// <summary>
        /// 根据主键返回，实例
        /// </summary>
        /// <param name="id">主键ID</param>
        /// <returns></returns>
        public T GetModelByID(object id)
        {
            Logger.Info("used GetModelByID(object id)");
            T model = null;
            try
            {
                model = iBaseService.FindByID(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                MessageBox.Show(ex.Message);
            }
            return model;
        }

        public bool Insert(T info)
        {
            Logger.Info("used Insert(T info)");
            bool ok = false;
            try
            {
                ok = iBaseService.Insert(info);
            }
            catch(Exception ex)
            {
                Logger.Error(ex, ex.Message);
                MessageBox.Show(ex.Message);
            }
            return ok;
        }

        public bool Insert(IEnumerable<T> list)
        {
            Logger.Info("used Insert(IEnumerable<T> list)");
            bool ok = false;
            try
            {
                ok = iBaseService.Insert(list);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                MessageBox.Show(ex.Message);
            }
            return ok;
        }

        public bool Update(T info)
        {
            Logger.Info("used Update(T info)");
            bool ok = false;
            try
            {
                ok = iBaseService.Update(info);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                MessageBox.Show(ex.Message);
            }
            return ok;
        }

        public bool Update(IEnumerable<T> list)
        {
            Logger.Info("used Update(IEnumerable<T> list");
            bool ok = false;
            try
            {
                ok = iBaseService.Update(list);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                MessageBox.Show(ex.Message);
            }
            return ok;
        }

        public bool Delete(T info)
        {
            Logger.Info("used Delete(T info)");
            bool ok = false;
            try
            {
                ok = iBaseService.Delete(info);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                MessageBox.Show(ex.Message);
            }
            return ok;
        }

        public bool Delete(IEnumerable<T> list)
        {
            Logger.Info("used Delete(IEnumerable<T> list)");
            bool ok = false;
            try
            {
                ok = iBaseService.Delete(list);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                MessageBox.Show(ex.Message);
            }
            return ok;
        }

        public bool Delete(object id)
        {
            Logger.Info("used Delete(object id)");
            bool ok = false;
            try
            {
                ok = iBaseService.Delete(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                MessageBox.Show(ex.Message);
            }
            return ok;
        }

        public bool DeleteAll()
        {
            Logger.Info("used DeleteAll()");
            bool ok = false;
            try
            {
                ok = iBaseService.DeleteAll();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                MessageBox.Show(ex.Message);
            }
            return ok;
        }


        #region 扩展
            
        #endregion
    }
}
