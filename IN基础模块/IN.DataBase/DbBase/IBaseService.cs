﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.DataBase
{
    /// <summary>
    /// 规范BaseService CURD
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseService<T>
    {
        /// <summary>
        /// 返回数据库所有的对象集合
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();
        /// <summary>
        /// 返回数据库所有的对象集合
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetBySQL(string sql);

        /// <summary>
        /// 查询数据库,返回指定ID的对象
        /// </summary>
        /// <param name="id">主键的值</param>
        /// <returns></returns>
        T FindByID(object id);
        /// <summary>
        /// 插入指定对象到数据库中
        /// </summary>
        /// <param name="info">指定的对象</param>
        /// <returns></returns>
        bool Insert(T info);
        /// <summary>
        /// 插入指定对象集合到数据库中
        /// </summary>
        /// <param name="list">指定的对象集合</param>
        /// <returns></returns>
        bool Insert(IEnumerable<T> list);
        /// <summary>
        /// 更新对象属性到数据库中
        /// </summary>
        /// <param name="info">指定的对象</param>
        /// <returns></returns>
        bool Update(T info);
        /// <summary>
        /// 更新指定对象集合到数据库中
        /// </summary>
        /// <param name="list">指定的对象集合</param>
        /// <returns></returns>
        bool Update(IEnumerable<T> list);
        /// <summary>
        /// 从数据库中删除指定对象
        /// </summary>
        /// <param name="info">指定的对象</param>
        /// <returns></returns>
        bool Delete(T info);
        /// <summary>
        /// 从数据库中删除指定对象集合
        /// </summary>
        /// <param name="list">指定的对象集合</param>
        /// <returns></returns>
        bool Delete(IEnumerable<T> list);
        /// <summary>
        /// 根据指定对象的ID,从数据库中删除指定对象
        /// </summary>
        /// <param name="id">对象的ID</param>
        /// <returns></returns>
        bool Delete(object id);
        /// <summary>
        /// 从数据库中删除所有对象
        /// </summary>
        /// <returns></returns>
        bool DeleteAll();
        /// <summary>
        /// 设定数据库
        /// </summary>
        void SetDataDB();
        void SetMesDataDB();

        string GetTableName();
        string GetTableYear();
        void SetTableYear(string year);


        #region 扩展
        int ExecuteBySql(string strSql, object dbParameter);
        /// <summary>
        /// 根据指定数据库执行sql语句
        /// </summary>
        /// <param name="databaseLinkId">数据库链接字符串</param>
        /// <param name="sql">sql语句</param>
        /// <param name="dbParameter">参数</param>
        void ExecuteBySql(string connString, string sql, object dbParameter = null);
        /// <summary>
        /// 根据指定数据库执行存储过程
        /// </summary>
        /// <param name="connString">数据库链接字符串</param>
        /// <param name="procName">存储过程</param>
        /// <param name="dbParameter">参数</param>
        void ExecuteByProc(string procName, object dbParameter = null);
        /// <summary>
        /// 根据指定数据库执行存储过程
        /// </summary>
        /// <param name="connString">数据库链接字符串</param>
        /// <param name="procName">存储过程</param>
        /// <param name="dbParameter">参数</param>
        void ExecuteByProc(string connString, string procName, object dbParameter = null);
        #endregion
    }
}
