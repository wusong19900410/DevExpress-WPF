﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.DataBase
{
    public interface IBaseIBLL<T>
    {
        List<T> GetListAll();
        T GetModelByID(object id);
        bool Insert(T info);
        bool Insert(IEnumerable<T> list);
        bool Update(T info);
        bool Update(IEnumerable<T> list);
        bool Delete(T info);
        bool Delete(IEnumerable<T> list);
        bool Delete(object id);
        bool DeleteAll();
    }
}
