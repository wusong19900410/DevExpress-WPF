﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.DataBase.Helper
{
    public class ConnectionFactory
    {


        /// <summary>
        /// 获取主数据库连接
        /// </summary>
        /// <returns></returns>
        private static IDbConnection CreateMainConnection()
        {
            IDbConnection DataConnection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["BaseDb"].ToString());

            return DataConnection;
        }

        /// <summary>
        /// 获取报表数据数据库连接
        /// </summary>
        /// <returns></returns>
        private static IDbConnection CreateDataConnection()
        {
            IDbConnection MainConnection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["BaseDb_Data"].ToString());
            return MainConnection;
        }

        public static IDbConnection CreateConnection(Database db)
        {
            //  IDbConnection connection = null;
            switch (db)
            {
                case Database.Main:
                    return CreateMainConnection();
                case Database.Data:
                    return CreateDataConnection();
                default:
                    return CreateMainConnection();

            }

        }

        public static IDbConnection CreateConnection(string  connectionString)
        {
            IDbConnection DataConnection = new System.Data.SqlClient.SqlConnection(connectionString);
            return DataConnection;
        }


    }
    public enum Database
    {
        Main,
        Data,
        MesData,
        HistoryData
    }
}
