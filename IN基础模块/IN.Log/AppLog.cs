﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newu.Log
{
    /// <summary>
    /// 日志模块
    /// zjq
    /// </summary>
    public class AppLog
    {
        /// <summary>
        /// 日志输出实例，基本上通过该实例输出日志
        /// </summary>
        public readonly static Logger Logger = LogManager.GetLogger("newu");
        public readonly static Logger MesLogger = LogManager.GetLogger("MesInterface");

        public readonly static List<Logger> loggers = new List<Logger>();


        /// <summary>
        /// 如果不写配置文件就要通过代码配置,写了配置文件代码省略不写
        /// </summary>
        public static void Init()
        {
            loggers.Add(Logger);
            loggers.Add(MesLogger);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Logger GetLoggerByName(string name)
        {
            var loger = loggers.FirstOrDefault(p => p.Name.Equals(name));
            if (loger == null)
            {
                loger = LogManager.GetLogger(name);
                loggers.Add(loger);
            }
            return loger;
        }

        /// <summary>
        /// 程序关闭前需手动关闭日志
        /// </summary>
        public static void Close()
        {
            loggers.Clear();
            NLog.LogManager.Shutdown();
        }

    }



}
