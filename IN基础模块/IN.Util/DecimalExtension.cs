﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Util
{
    public static class DecimalExtension
    {
        /// <summary>
        /// decimal保留指定位数小数
        /// </summary>
        /// <param name="num">原始数量</param>
        /// <param name="scale">保留小数位数</param>
        /// <returns>截取指定小数位数后的数量字符串</returns>
        public static string ToString(this decimal num, int scale)
        {
            string numToString = num.ToString();

            int index = numToString.IndexOf(".");
            int length = numToString.Length;

            if (index != -1)
            {
                return string.Format("{0}.{1}",
                    numToString.Substring(0, index),
                    numToString.Substring(index + 1, Math.Min(length - index - 1, scale)));
            }
            else
            {
                if (scale == 0)
                {
                    return numToString;
                }
                string formatestr = ".".PadRight(scale + 1, '0');

                return numToString + formatestr;
            }
        }

        /// <summary>
        /// decimal保留指定位数小数
        /// </summary>
        /// <param name="num">原始数量</param>
        /// <param name="scale">保留小数位数</param>
        /// <returns>截取指定小数位数后的数量字符串</returns>
        public static string ToString(this decimal num, int scale, bool noreserveZero)
        {
            if (!noreserveZero)
            {
                string numToString = num.ToString();

                int index = numToString.IndexOf(".");
                int length = numToString.Length;

                if (index != -1)
                {
                    return string.Format("{0}.{1}",
                        numToString.Substring(0, index),
                        numToString.Substring(index + 1, Math.Min(length - index - 1, scale)));
                }
                else
                {
                    if (scale == 0)
                    {
                        return numToString;
                    }
                    string formatestr = ".".PadRight(scale + 1, '0');

                    return numToString + formatestr;
                }
            }
            else
            { //不留0
                string formatestr = "0.";
                if (scale > 0)
                {
                    for (int i = 0; i < scale; i++)
                    {
                        formatestr += "#";
                    }
                    return num.ToString(formatestr);
                }
                else
                {
                    return num.ToString("0");
                }
            }

        }


        /// <summary>
        /// decimal保留指定位数小数
        /// </summary>
        /// <param name="num">原始数量</param>
        /// <param name="scale">保留小数位数</param>
        /// <returns>截取指定小数位数后的数量字符串</returns>
        public static string ToString(this float num, int scale)
        {
            string numToString = num.ToString();

            int index = numToString.IndexOf(".");
            int length = numToString.Length;

            if (index != -1)
            {
                return string.Format("{0}.{1}",
                    numToString.Substring(0, index),
                    numToString.Substring(index + 1, Math.Min(length - index - 1, scale)));
            }
            else
            {
                if (scale == 0)
                {
                    return numToString;
                }
                string formatestr = ".".PadRight(scale + 1, '0');

                return numToString + formatestr;
            }
        }

        /// <summary>
        /// decimal保留指定位数小数
        /// </summary>
        /// <param name="num">原始数量</param>
        /// <param name="scale">保留小数位数</param>
        /// <returns>截取指定小数位数后的数量字符串</returns>
        public static string ToString(this double num, int scale)
        {
            string numToString = num.ToString();

            int index = numToString.IndexOf(".");
            int length = numToString.Length;

            if (index != -1)
            {
                return string.Format("{0}.{1}",
                    numToString.Substring(0, index),
                    numToString.Substring(index + 1, Math.Min(length - index - 1, scale)));
            }
            else
            {
                if (scale == 0)
                {
                    return numToString;
                }
                string formatestr = ".".PadRight(scale + 1, '0');

                return numToString + formatestr;
            }
        }
    }
}
