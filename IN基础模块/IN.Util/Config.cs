﻿using System;
using System.Configuration;

namespace IN.Util
{
    /// <summary>
    /// 版 本 NEWU 1.0  北京万向 http://www.newu.com.cn/
    /// Copyright (c) 2013-2018 
    /// 创建人：吴松
    /// 日 期：2017.03.07
    /// 描 述：Config文件操作
    /// </summary>
    public class Config
    {
        /// <summary>
        /// 根据Key取Value值
        /// </summary>
        /// <param name="key"></param>
        public static string GetValue(string key)
        {

            return ConfigurationManager.AppSettings[key].ToString().Trim();
        }
        
    }
}
