﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Util
{

    public class Weight
    {
        public string Scale { get; set; }
        public int Decimal { get; set; }
    }

    public class Mix
    {
        public string Var { get; set; }
        public int Decimal { get; set; }
    }
    public class OpenMill
    {
        public string Var { get; set; }
        public int Decimal { get; set; }
    }

    public class Root
    {
        public List<Weight> Weight { get; set; }
        public List<Mix> Mix { get; set; }
        public List<OpenMill> OpenMill { get; set; }
    }
}
