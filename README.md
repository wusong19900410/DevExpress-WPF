# DevExpress WPF
 **不会上次代码，正在研究** 

WPF工业软件 介绍
基于DevExpressWpf 实现工业工控软件

#### 软件架构
WPF
MVVM框架
DevExpress20.1
SQL-Server


#### 安装教程

1.  安装DevExpress20.1环境、安装VS环境，安装SQL-Server2012+数据库环境
2.  加载数据库database/WPF_V1.sql
3.  打开VS运行软件

#### 功能说明

1.  系统管理
1.1 数据字典
1.2 系统功能
2.  权限管理
![菜单管理](https://images.gitee.com/uploads/images/2020/0807/105039_4dbff46d_6574017.png "屏幕截图.png")
![菜单视图和按钮配置](https://images.gitee.com/uploads/images/2020/0807/105201_d40b2c74_6574017.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
