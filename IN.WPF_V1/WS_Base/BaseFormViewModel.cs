﻿using DevExpress.XtraEditors;
using IN.Interface;
using System;
namespace IN.DX.WS_Base
{
    //[POCOViewModel]
    public class BaseFormViewModel : IDataBase
    {
        private object _contentControl;
        public object ContentControl
        {
            get
            {
                return _contentControl;
            }
            set
            {
                _contentControl = value;
                //RaisePropertyChangedEvent("ContentControl");
            }
        }

    }
}

