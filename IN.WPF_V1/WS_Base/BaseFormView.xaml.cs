﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IN.DX.WS_Base
{
    /// <summary>
    /// Interaction logic for BaseFormView.xaml
    /// </summary>
    public partial class BaseFormView : UserControl
    {
        public BaseFormView()
        {
            InitializeComponent();
            InitFormDesigner();
        }


        private void InitFormDesigner()
        {
            //XtraForm fm = new MainForm();

            //fm.TopLevel = false;
            //fm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            //fm.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation;
            //fm.WindowState = System.Windows.Forms.FormWindowState.Normal;
            //fm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            //fm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;


        }


    }
}
