﻿using IN.DX.Common.Unity;
using IN.DX.WS_Home;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace IN.DX
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private System.Threading.Mutex mutex;

        public App()
        {
            this.Startup += new StartupEventHandler(App_Startup);
        }


        private void App_Startup(object sender, StartupEventArgs e)
        {
            bool ret;
            mutex = new System.Threading.Mutex(true, "工业开发平台", out ret);

            if (!ret)
            {
                MessageBox.Show("程序已打开", "", MessageBoxButton.OK, MessageBoxImage.Information);
                Environment.Exit(0);
            }

        }

        protected override void OnStartup(StartupEventArgs e)
        {
            //IOC接口注册
            BootStrapper.Initialize();


            Application.Current.ShutdownMode = System.Windows.ShutdownMode.OnExplicitShutdown;
            Login window = new Login();
            bool? dialogResult = window.ShowDialog();
            if ((dialogResult.HasValue == true) &&
                (dialogResult.Value == true))
            {
                base.OnStartup(e);
                Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
            }
            else
            {
                this.Shutdown();
            }

        }
    }
}
