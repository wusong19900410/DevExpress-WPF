﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.DX.Common.Corelib
{
    public class MultiQuery
    {
        private string _QueryName;
        /// <summary>
        /// 查询字段
        /// </summary>
        private string _QueryField;
        /// <summary>
        /// 查询数值
        /// </summary>
        private string _QueryValue;

        public MultiQuery(string queryName,string queryField, string queryValue)
        {
            _QueryName = queryName;
            _QueryField = queryField;
            _QueryValue = queryValue;
        }
        public string QueryName { get => _QueryName; set => _QueryName = value; }
        public string QueryField { get => _QueryField; set => _QueryField = value; }
        public string QueryValue { get => _QueryValue; set => _QueryValue = value; }

    }
}
