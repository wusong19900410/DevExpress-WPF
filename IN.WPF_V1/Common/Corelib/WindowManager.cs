﻿using IN.DX.Common.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace IN.DX.Common.Corelib
{
    /// <summary>
    /// Industry Software
    /// Copyright (c) 2020-2022
    /// 创建人：吴松
    /// 日 期：2020.07.09
    /// 描 述：窗体管理类
    /// </summary>
    public static class WindowManager
    {
        private static Hashtable _RegisterWindow = new Hashtable();

        public static void Regiter<T>(string key)
        {
            _RegisterWindow.Add(key, typeof(T));
        }
        public static void Regiter(string key, Type t)
        {
            if (!_RegisterWindow.ContainsKey(key))
                _RegisterWindow.Add(key, t);
        }

        public static void Remove(string key)
        {
            if (_RegisterWindow.ContainsKey(key))
                _RegisterWindow.Remove(key);
        }

        public static void ShowDialog(string key, object VM)
        {
            if (!_RegisterWindow.ContainsKey(key))
            {
                throw (new Exception("没有注册此键！"));
            }

            var uc = (UserControl)Activator.CreateInstance((Type)_RegisterWindow[key]);
            uc.DataContext = VM;
            var win =new Window();
            win.Content = uc;
            //win.Owner = Application.Current.MainWindow;
            win.ShowDialog();
        }

    }


    public static class WindowExt
    {
        public static void Register(this UserControl win, string key)
        {
            WindowManager.Regiter(key, win.GetType());
        }

        public static void Register(this UserControl win, string key, Type t)
        {
            WindowManager.Regiter(key, t);
        }

        public static void Register<T>(this UserControl win, string key)
        {
            WindowManager.Regiter<T>(key);
        }

    }
}
