﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.DX.Common.Corelib
{
    public class ToolBarDefault<T> :ViewModelBase
    {
        private string buttunName;
        private byte[] buttonIcon;
        private DelegateCommand<T> command;
        /// <summary>
        /// 按钮名称
        /// </summary>
        public string ButtunName {
            get { return buttunName; }
            set { buttunName = value; RaisePropertyChanged(); }
        }

        public byte[] ButtonIcon
        {
            get { return buttonIcon; }
            set { buttonIcon = value; RaisePropertyChanged(); }
        }
        /// <summary>
        /// 按钮命令
        /// </summary>
        public DelegateCommand<T> Command
        {
            get { return command; }
            set { command = value; RaisePropertyChanged(); }
        }
    }
}
