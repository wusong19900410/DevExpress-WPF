﻿using IN.Interface;
using IN.DX.Common.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using IN.Module;
using System.Windows;
using IN.DataBase;
using System.Windows.Controls;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;

namespace IN.DX.Common.Corelib
{
    [POCOViewModel]
    public partial class DXBaseViewModel<T> : DevExpress.Mvvm.ViewModelBase
    {
        private T _Entity;
        public T Entity
        {
            get { return _Entity; }
            set { _Entity = value; RaisePropertyChanged(); }
        }

        #region 功能按钮
        private ObservableCollection<ToolBarDefault<T>> _ButtonDefaults;

        /// <summary>
        /// 按钮集合
        /// </summary>
        public ObservableCollection<ToolBarDefault<T>> ButtonDefaults
        {
            get { return _ButtonDefaults; }
            set { _ButtonDefaults = value; }
        }
        #endregion

        #region 基类实现
        /// <summary>
        /// 初始化
        /// </summary>
        public virtual void InitViewModel()
        {
            ButtonDefaults = new ObservableCollection<ToolBarDefault<T>>();
            this.SetDefaultButton(); //默认功能按钮
        }
        /// <summary>
        /// 设置默认按钮
        /// </summary>
        public virtual void SetDefaultButton()
        {
            //ButtonDefaults.Add(new ToolBarDefault<T>() { ButtunName = "关闭", Command = new RelayCommand<T>(t => { this.Close(); }) });
        }
        /// <summary>
        /// 动态加载按钮工具栏 吴松
        /// </summary>
        /// <param name="buttonList"></param>
        public void SetDBButton(string menuID,string roleID)
        {
            Sys_ButtonBll buttonBll = new Sys_ButtonBll();
            List<Sys_ButtonEntity> ButtonList = buttonBll.GetListByMenuID(menuID, roleID);
            if (ButtonList != null)
            {
                for (int i = 0; i < ButtonList.Count; i++)
                {
                    string ActionName = ButtonList[i].ButtonCommand;
                    ButtonDefaults.Add(new ToolBarDefault<T>()
                    {
                        ButtunName = ButtonList[i].ButtonName,
                        ButtonIcon=ButtonList[i].ButtonIcon,
                        Command = new DelegateCommand<T>(t =>
                        {
                            MethodInfo[] methods = this.GetType().GetMethods();
                            MethodInfo method = this.GetType().GetMethod(ActionName);//此处动态传参存在问题20200712吴松
                            if (method != null)
                            {
                                if (method.IsGenericMethod) //如果是泛型方法
                                {
                                    MethodInfo methodF = method?.MakeGenericMethod(typeof(T)); //判断null防止没写空异常
                                    methodF?.Invoke(this, new object[] { _Entity });
                                }
                                else
                                {
                                    method?.Invoke(this, new object[] { });
                                }
                            }


                        }, true)
                    });
                }
            }
        }
       
        #endregion

        #region 功能命令

        private DelegateCommand<T> _AddCommand;
        private DelegateCommand<T> _EditCommand;
        private DelegateCommand<T> _DelCommand;
        private DelegateCommand<object> _MultiQueryCommand;
        private DelegateCommand<object> _ResetPmCommand;
        private DelegateCommand _QueryCommand;
        private DelegateCommand _ResetCommand;
        private DelegateCommand _CloseCommand;

        /// <summary>
        /// 构造函数
        /// </summary>
        public DXBaseViewModel()
        {
            InitViewModel();
        }

        //private RelayCommand<ContextMenuModel> _ExcuteCommand;


        /// <summary>
        /// 新增
        /// </summary>
        public DelegateCommand<T> AddCommand
        {
            get
            {
                if (_AddCommand == null)
                {
                    _AddCommand = new DelegateCommand<T>(t => Add(t));
                }
                return _AddCommand;
            }
            set { _AddCommand = value; }
        }

        /// <summary>
        /// 编辑
        /// </summary>
        public DelegateCommand<T> EditCommand
        {
            get
            {
                if (_EditCommand == null)
                {
                    _EditCommand = new DelegateCommand<T>(t => Edit(t));
                }
                return _EditCommand;
            }
            set { _EditCommand = value; }
        }

        /// <summary>
        /// 删除
        /// </summary>
        public DelegateCommand<T> DelCommand
        {
            get
            {
                if (_DelCommand == null)
                {
                    _DelCommand = new DelegateCommand<T>(t => Del(t));
                }
                return _DelCommand;
            }
            set { _DelCommand = value; }
        }

        /// <summary>
        /// 根据多个条件查询
        /// </summary>
        public DelegateCommand<Object> MultiQueryCommand
        {
            get
            {
                if (_MultiQueryCommand == null)
                {
                    _MultiQueryCommand = new DelegateCommand<Object>(t => MultiQuery(t));
                }
                return _MultiQueryCommand;
            }
            set { _MultiQueryCommand = value; }
        }

        /// <summary>
        /// 重置查询条件
        /// </summary>
        public DelegateCommand<Object> ResetPmCommand
        {
            get
            {
                if (_ResetPmCommand == null)
                {
                    _ResetPmCommand = new DelegateCommand<Object>(t => ResetPm(t));
                }
                return _ResetPmCommand;
            }
            set { _ResetPmCommand = value; }
        }
        /// <summary>
        /// 查询
        /// </summary>
        public DelegateCommand QueryCommand
        {
            get
            {
                if (_QueryCommand == null)
                {
                    _QueryCommand = new DelegateCommand(() => Query());
                }
                return _QueryCommand;
            }
            set { _QueryCommand = value; }
        }

        /// <summary>
        /// 查询
        /// </summary>
        public DelegateCommand ResetCommand
        {
            get
            {
                if (_ResetCommand == null)
                {
                    _ResetCommand = new DelegateCommand(() => Reset());
                }
                return _ResetCommand;
            }
            set { _ResetCommand = value; }
        }
        /// <summary>
        /// 关闭
        /// </summary>
        public DelegateCommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new DelegateCommand(() => Close());
                }
                return _CloseCommand;
            }
            set { _CloseCommand = value; }
        }
        #endregion

    }


    /// <summary>
    /// 主窗口/分布基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public partial class DXBaseViewModel<T> : IDataOperation
    {


        #region IDataOperation

        /// <summary>
        /// 新增
        /// </summary>
        public virtual void Add<TModel>(TModel model) { }

        /// <summary>
        /// 编辑
        /// </summary>
        public virtual void Edit<TModel>(TModel model){ }

        /// <summary>
        /// 删除
        /// </summary>
        public virtual void Del<TModel>(TModel model) { }

        /// <summary>
        /// 查询
        /// </summary>
        public virtual void Query(){ }

        /// <summary>
        /// 根据多条件查询数据
        /// </summary>
        public virtual void MultiQuery(object model) 
        { 
        
        }
        /// <summary>
        /// 
        /// </summary>
        public virtual void Close()
        {
            //关闭弹出窗体
        }
        public virtual void Reset()
        { 
            throw new NotImplementedException();

        }

        /// <summary>
        /// 重置查询条件
        /// </summary>
        public virtual void ResetPm(object model) 
        {
            if(model!=null)
            {
                var obj = (object[])model;
                for (int i = 0; i < obj.Length; i++)
                {
                    Type tp = obj[i].GetType();
                    switch (tp.Name)
                    {
                        case "DatePicker":
                            (obj[i] as DatePicker).Text = "";
                            break;
                        case "TextBox":
                            (obj[i] as TextBox).Text = "";
                            break;
                        case "ComboBox":
                            (obj[i] as ComboBox).Text = "";
                            break;
                        default:
                            break;
                    }
                }
            }

        }

        #endregion

    }
}
