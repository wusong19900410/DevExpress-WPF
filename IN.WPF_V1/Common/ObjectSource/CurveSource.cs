﻿using DevExpress.DataAccess.ObjectBinding;
using DevExpress.XtraCharts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.DX.Common
{
    public class CurveModel
    {
        public string OrderID { get; set; }
        public string FormulaName { get; set; }
        public int Num { get; set; }
        public string Lot { get; set; }
        public int PlanQty { get; set; }
        public int FactOrder { get; set; }
        public DateTime RealTime { get; set; }
        public double Temp { get; set; }
        public double Power { get; set; }
        public double Press { get; set; }
        public double Energy { get; set; }
        public double Speed { get; set; }
        public double v1 { get; set; }
        public double v2 { get; set; }
        public double v3 { get; set; }
        public double v4 { get; set; }
        public double v5 { get; set; }
        public double v6 { get; set; }
    }

    public class CurveSource
    {
        public List<CurveModel> series { get; set; }

    }

}
