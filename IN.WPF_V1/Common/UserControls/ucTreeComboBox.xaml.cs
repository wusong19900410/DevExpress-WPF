﻿using IN.DX.WS_Home;
using IN.Module;
using IN.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IN.DX.Common.UserControls
{
    /// <summary>
    /// ucTreeComboBox.xaml 的交互逻辑
    /// </summary>
    public partial class ucTreeComboBox : UserControl
    {

        public string selectedName { get; set; }

        #region 自定义属性

        #region SelectedId 属性
        public string SelectedId 
        {
            get { return (string)GetValue(SelectedIdProperty); }
            set { SetValue(SelectedIdProperty, value); }
        }

        public static readonly DependencyProperty SelectedIdProperty =
            DependencyProperty.Register("SelectedId", typeof(string), typeof(ucTreeComboBox), new FrameworkPropertyMetadata()
            {
                PropertyChangedCallback = OnSelectedIdChanged,
                BindsTwoWayByDefault = true
            });

        private static void OnSelectedIdChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != e.NewValue)
            {
                // code to be executed on value update
            }
        }
        #endregion


        #region ItemsSource 属性

        public ObservableCollection<TreeModel> ItemsSource
        {
            get { return (ObservableCollection<TreeModel>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(ObservableCollection<TreeModel>), typeof(ucTreeComboBox), new FrameworkPropertyMetadata()
            {
                PropertyChangedCallback = OnItemsSourceChanged,
                BindsTwoWayByDefault = true
            });

        private static void OnItemsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != e.NewValue)
            {
                // code to be executed on value update
            }
        }
        #endregion


        #region SelectedItem属性
        public TreeModel SelectedItem
        {
            get{ return (TreeModel)GetValue(SelectedItemProperty); }
            set{ SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(TreeModel), typeof(ucTreeComboBox));

        #endregion


        #endregion
        public ucTreeComboBox()
        {

            InitializeComponent();
        }

        private void lftTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                SelectedItem = (TreeModel)e.NewValue;
                selectedName = SelectedItem.text;
                SelectedId = SelectedItem.id;
            }
            catch (Exception)
            {
                //logger.Error(ex.ToString());
            }
        }
        private void TreeComboBox_DropDownClosed(object sender, EventArgs e)
            {
            TreeComboBox.Items[0] = selectedName;
            TreeComboBox.SelectedItem = TreeComboBox.Items[0];
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            TreeModel TreeModel = null;
            if (ItemsSource !=null)
            {
                this.TreeComboBox.DataContext = ItemsSource;
                TreeModel = TreeDataMake.FindNode(ItemsSource, SelectedId);
            }

            if (TreeModel != null)
            {
                selectedName = TreeModel.text;
                TreeComboBox.Items[0] = selectedName;
                TreeComboBox.SelectedItem = TreeComboBox.Items[0];
            }

        }

    }
}
