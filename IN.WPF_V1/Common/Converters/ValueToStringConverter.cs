﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace IN.DX.Common.Converters
{
    public class ValueToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int a = (int)value;
            if (a == 0)
            {
                return "页面";
            }
            else
            {
                return "弹窗";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string temp = value as string;
            if(temp == "页面")
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
    }
}
