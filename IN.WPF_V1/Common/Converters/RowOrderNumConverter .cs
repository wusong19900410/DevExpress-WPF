﻿using DevExpress.Xpf.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace IN.DX.Common.Converters
{
    public class RowOrderNumConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var rowHandle = value as RowHandle;
            if (rowHandle != null)
            {
                int num = rowHandle.Value;
                return (num + 1);
            }
            return "2";
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return 0;
        }
    }
}
