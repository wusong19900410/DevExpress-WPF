﻿using IN.Module;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
namespace IN.DX.Common.Converters
{
    /// <summary>
    /// 可以静态态加载数据  用于数据转换  与LocalSource动态态相比 
    /// </summary>
    public class BaseConverter : IValueConverter
    {
        public static List<TB_DepartmentEntity> DepartmentList = new TB_DepartmentBll().GetListAll();
        private static List<TB_RoleEntity> RoleList = new TB_RoleBll().GetListAll();
        public static List<TB_DataItemDetailEntity> DataItemList = new TB_DataItemDetailBll().GetListAll();
           
        
        public BaseConverter()
        {

        }

        private enum ParamType
        {
            Device,//设备 
            DevicePartType,//设备部件类型
            Department,//部门
            Role,//角色
            MaterialType,//物料类型

        }


        /// <summary>
        /// 进行源属性传给目标属性的特定转化
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            object res;
            try
            {
                switch ((ParamType)Enum.Parse(typeof(ParamType), parameter.ToString()))
                {
                    case ParamType.Department:
                        res = DepartmentList.Where(a => a.DepartmentID.Equals(value.ToString())).FirstOrDefault().DepartmentName;
                        break;
                    case ParamType.Role:
                        res = RoleList.Where(a => a.RoleID.Equals(value.ToString())).FirstOrDefault().RoleName;
                        break;
                    case ParamType.MaterialType:
                        res = DataItemList.Where(a => a.F_ItemCode.Equals(value.ToString())).FirstOrDefault().F_ItemName;
                        break;
                    default:
                        res = new object();
                        break;
                }
                return res;
            }
            catch (Exception)
            {
                //todo 记录日志
                return new object();
            }
        }
        /// <summary>
        /// 进行目标属性传给源属性的特定转化
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
