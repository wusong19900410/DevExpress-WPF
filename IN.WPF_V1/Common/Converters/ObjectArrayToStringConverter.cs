﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;

namespace IN.DX.Common.Converters
{

    [ValueConversion(typeof(object[]), typeof(String))]
    public class ObjectArrayToStringConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Array res = value as Array;
            if (res == null) return null;
            string separator = parameter.ToString();
            string strResult = "";
            for (int i = 0; i < res.Length; i++)
            {
                if (i==0)
                    strResult = res.GetValue(i).ToString();
                else
                    strResult += separator + res.GetValue(i).ToString();
            }
            return strResult;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value==null) return null;
            string separator = parameter.ToString();
            var array = Regex.Split(value.ToString(), separator, RegexOptions.None);
            return array;
        }

    }
}
