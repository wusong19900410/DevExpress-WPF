﻿using IN.Module;
using System.Collections.Generic;

namespace IN.DX.Common.Converters
{

    //可以动态加载数据  用于数据转换  与BaseConverter静态相比 
    public  class BaseConverterDynamic
    {

        private  readonly Sys_WorkShopBll WorkShopBll = new Sys_WorkShopBll();
        private  readonly Sys_CompanyBll CompanyBll = new Sys_CompanyBll();
        private  readonly TB_DepartmentBll DepartmentBll = new TB_DepartmentBll();
        private  readonly TB_RoleBll RoleBll = new TB_RoleBll();
        private  readonly TB_DataItemDetailBll DataItemDetailBll = new TB_DataItemDetailBll();

        //公司
        public  List<Sys_CompanyEntity> CompanyList
        {
            get
            {
                return CompanyBll.GetListAll() ?? new List<Sys_CompanyEntity>();
            }
        }

        //车间
        public  List<Sys_WorkShopEntity> WorkShopList
        {
            get
            {
                return WorkShopBll.GetListAll() ?? new List<Sys_WorkShopEntity>();
            }
        }

        //部门
        public  List<TB_DepartmentEntity> DepartmentList
        {
            get
            {

                return DepartmentBll.GetListAll() ?? new List<TB_DepartmentEntity>();
            }
        }

        //角色
        public  List<TB_RoleEntity> RoleList
        {
            get
            {

                return RoleBll.GetListAll() ?? new List<TB_RoleEntity>();
            }
        }

        //物料类型
        public  List<TB_DataItemDetailEntity> MaterialTypeList
        {
            get
            {
                return DataItemDetailBll.GetListAllByItemCode("MaterialCategory") ?? new List<TB_DataItemDetailEntity>();
            }
        }

        //控制方式
        public  List<TB_DataItemDetailEntity> ControlModeList
        {
            get
            {
                return DataItemDetailBll.GetListAllByItemCode("ControlModeCategory") ?? new List<TB_DataItemDetailEntity>();
            }
        }


    }
}
