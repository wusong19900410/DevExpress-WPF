﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IN.DX.Common.Unity3D
{
    public class Unity3dContainer
    {
        [DllImport("User32.dll")]
        static extern bool MoveWindow(IntPtr handle, int x, int y, int width, int height, bool redraw);

        internal delegate int WindowEnumProc(IntPtr hwnd, IntPtr lparam);
        [DllImport("user32.dll")]
        internal static extern bool EnumChildWindows(IntPtr hwnd, WindowEnumProc func, IntPtr lParam);

        [DllImport("user32.dll")]
        static extern int SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr SetParent(IntPtr child, IntPtr newParent);

        private Process process;
        private IntPtr unityHWND = IntPtr.Zero;

        private const int WM_ACTIVATE = 0x0006;
        private readonly IntPtr WA_ACTIVE = new IntPtr(1);
        private readonly IntPtr WA_INACTIVE = new IntPtr(0);

        private System.Windows.Forms.Panel _hostPanel;
        private readonly ManualResetEvent _eventDone = new ManualResetEvent(false);
        private Process _process = null;
        internal IntPtr _embededWindowHandle;

        public Unity3dContainer(System.Windows.Forms.Panel panel)
        {
            this._hostPanel = panel;
            this._hostPanel.Resize += _hostPanel_Resize;
        }

        private void _hostPanel_Resize(object sender, EventArgs e)
        {
            SetBounds();
        }

        public void ActivateWindow()
        {
            if (_process == null)
                return;

            if (_process.MainWindowHandle == IntPtr.Zero)
                return;

            SendMessage(_process.MainWindowHandle, WM_ACTIVATE, WA_ACTIVE, IntPtr.Zero);
        }

        public void SetBounds()
        {
            SetBounds(_hostPanel.Width, _hostPanel.Height);
        }

        public void SetBounds(int width, int height)
        {
            if (_process == null)
                return;

            if (_process.MainWindowHandle == IntPtr.Zero)
                return;

            if (width <= 0 || height <= 0)
                return;

            MoveWindow(_process.MainWindowHandle, 0, 0, width, height, true);

            ActivateWindow();//激活
        }

        public bool StartAndEmbedProcess(string processPath)
        {
            if (null != _process)
                return true;

            var isStartAndEmbedSuccess = false;
            _eventDone.Reset();

            // Start the process 
            ProcessStartInfo info = new ProcessStartInfo(processPath);
            info.WindowStyle = ProcessWindowStyle.Maximized;//默认最大化，不弹出界面。
            info.Arguments = $"-popupwindow";//Unity的命令行参数

            _process = Process.Start(info);

            if (_process == null)
            {
                return false;
            }

            // Wait for process to be created and enter idle condition 
            _process.WaitForInputIdle();

            // Get the main handle
            var thread = new Thread(() =>
            {
                while (true)
                {
                    if (_process.MainWindowHandle != (IntPtr)0)
                    {
                        _eventDone.Set();
                        break;
                    }
                    Thread.Sleep(10);
                }
            });
            thread.Start();

            //嵌入进程
            if (_eventDone.WaitOne(10000))
            {
                isStartAndEmbedSuccess = EmbedApp(_process);
                if (!isStartAndEmbedSuccess)
                {
                    CloseApp(_process);
                }
            }
            return isStartAndEmbedSuccess;
        }

        public bool EmbedExistProcess(Process process)
        {
            _process = process;
            return EmbedApp(process);
        }

        /// <summary>
        /// 将外进程嵌入到当前程序
        /// </summary>
        /// <param name="process"></param>
        private bool EmbedApp(Process process)
        {
            //是否嵌入成功标志，用作返回值
            var isEmbedSuccess = false;
            //外进程句柄
            var processHwnd = process.MainWindowHandle;
            //容器句柄
            var panelHwnd = _hostPanel.Handle;

            if (processHwnd != (IntPtr)0 && panelHwnd != (IntPtr)0)
            {
                //把本窗口句柄与目标窗口句柄关联起来
                var setTime = 0;
                while (!isEmbedSuccess && setTime < 50)
                {
                    // Put it into this form
                    isEmbedSuccess = SetParent((IntPtr)processHwnd, (IntPtr)panelHwnd) != (IntPtr)0;
                    Thread.Sleep(10);
                    setTime++;
                }

                // Remove border and whatnot
                //Win32Api.SetWindowLong(processHwnd, Win32Api.GWL_STYLE, Win32Api.WS_CHILDWINDOW | Win32Api.WS_CLIPSIBLINGS | Win32Api.WS_CLIPCHILDREN | Win32Api.WS_VISIBLE);

                SetBounds();

                //Move the window to overlay it on this window
                //Win32Api.MoveWindow(_process.MainWindowHandle, 0, 0, (int)ActualWidth, (int)ActualHeight, true);
            }

            if (isEmbedSuccess)
            {
                _embededWindowHandle = _process.MainWindowHandle;
            }

            return isEmbedSuccess;
        }

        /// <summary>
        /// 关闭进程
        /// </summary>
        /// <param name="process"></param>
        private void CloseApp(Process process)
        {
            if (process != null && !process.HasExited)
            {
                process.Kill();
            }
        }

        public void CloseProcess()
        {
            CloseApp(_process);
            _process = null;
        }
    }
}
