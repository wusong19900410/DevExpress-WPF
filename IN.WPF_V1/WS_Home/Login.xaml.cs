﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Mvvm;
using DevExpress.Xpf.Core;
using DXApplication1;
using IN.Module;

namespace IN.DX.WS_Home
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        private readonly TB_UserBll UserBll =new TB_UserBll();
        public Login()
        {
            InitializeComponent();
        }

        private void Btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Btn_Login_Click(object sender, RoutedEventArgs e)
        {
            string strUser = this.txtUser.Text;
            string strPassword = this.txtPassword.Password;
            if(string.IsNullOrEmpty(strUser) || string.IsNullOrEmpty(strPassword))
            {
                this.resultMsg.Content = "请输入账号和密码!";
                return;
            }

            try
            {
                TB_UserEntity User = (TB_UserEntity)UserBll.GetListAll().FirstOrDefault(t => t.UserCode.Equals(strUser));
                if(User == null)
                {
                    this.resultMsg.Content = "系统中不存在该账号!";
                    return;
                }
                if(!User.UserPassword.Equals(strPassword))
                {
                    this.resultMsg.Content = "登录密码输入错误!";
                    return;
                }
                //登录成功后，切换系统用户到全局变量
                GlobalBLL.SysUser = User;
                this.resultMsg.Content = "登录成功...!";
                Messenger.Default.Send(User.UserID, "LoginIn_Success");
                this.DialogResult = true;

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

    }
}
