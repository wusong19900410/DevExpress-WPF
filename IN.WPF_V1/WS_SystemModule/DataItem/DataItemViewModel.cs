﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.DX.Common.Corelib;
using IN.Module;
using System.Collections.ObjectModel;
using IN.Interface;
using DevExpress.Mvvm.POCO;

namespace IN.DX.WS_SystemModule
{
    [POCOViewModel]
    public class DataItemViewModel : DXBaseViewModel<TB_DataItemEntity>,IDataOperation
    {
        #region 属性
        private readonly TB_DataItemBll DataItemBll = new TB_DataItemBll();

        private ObservableCollection<TB_DataItemEntity> _DataItemList;
        public ObservableCollection<TB_DataItemEntity> DataItemList
        {
            get { return _DataItemList; }
            set
            {
                _DataItemList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region 构造函数
        public DataItemViewModel()
        {
            DataItemList = new ObservableCollection<TB_DataItemEntity>(DataItemBll.GetListAll());
        }

        public static DataItemViewModel Create()
        {
            return ViewModelSource.Create(() => new DataItemViewModel());
        }
        #endregion

        #region 用户操作 增删改查
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Add<TModel>(TModel model)
        {
            var entity = new TB_DataItemEntity();
            MessageResult result = DialogService.ShowDialog(MessageButton.OKCancel, "字典编辑", "DataItemDialogView", DataItemDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.OK))
            {
                if (DataItemBll.Insert(entity))
                    DataRefrash();
            }

        }

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Edit<TModel>(TModel model)
        {

            if (model == null)
                return;
            var entity = model as TB_DataItemEntity;
            MessageResult result = DialogService.ShowDialog(MessageButton.YesNo, "字典编辑", "DataItemDialogView", DataItemDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.Yes))
            {
                if (DataItemBll.Update(entity))
                    DataRefrash();
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Del<TModel>(TModel model)
        {
            if (model == null)
                return;
            var entity = model as TB_DataItemEntity;
            MessageResult result = MessageBoxService.ShowMessage("是否确认删除【" + entity.F_ItemName + "】该项?", "提醒", MessageButton.YesNo);
            if (result.Equals(MessageResult.Yes))
            {
                if (DataItemBll.Delete(entity))
                    DataRefrash();
            }

        }

        #endregion

        #region 服务接口
        [ServiceProperty(SearchMode = ServiceSearchMode.PreferParents)]

        protected virtual IDialogService DialogService { get { return this.GetService<IDialogService>(); } }
        protected virtual IMessageBoxService MessageBoxService { get { return ServiceContainer.GetService<IMessageBoxService>(); } }
        protected virtual ICurrentWindowService CurrentWindowService { get { return null; } }
        #endregion

        #region 刷新数据
        private void DataRefrash()
        {
            DataItemList = null;
            DataItemList = new ObservableCollection<TB_DataItemEntity>(DataItemBll.GetListAll());
        }
        #endregion
    }
}