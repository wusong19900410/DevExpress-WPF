﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.Module;
using DevExpress.Mvvm.POCO;

namespace IN.DX.WS_SystemModule
{
    [POCOViewModel]
    public class DataItemDialogViewModel
    {
        private readonly TB_DataItemBll DataItemBll = new TB_DataItemBll();
        #region 属性

        //属性：部门
        public virtual TB_DataItemEntity Entity { get; set; }

        #endregion

        #region 构造函数 无参数
        protected DataItemDialogViewModel()
        {
            Entity = new TB_DataItemEntity();
        }
        public static DataItemDialogViewModel Create()
        {
            return ViewModelSource.Create(() => new DataItemDialogViewModel());
        }
        #endregion

        #region 构造函数 一个参数
        protected DataItemDialogViewModel(TB_DataItemEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity", "entity is null.");
            Load(entity);
        }
        private void Load(TB_DataItemEntity entity)
        {
            this.Entity = entity;
        }
        public static DataItemDialogViewModel Create(TB_DataItemEntity entity)
        {
            return ViewModelSource.Create(() => new DataItemDialogViewModel(entity));
        }
        #endregion
    }
}