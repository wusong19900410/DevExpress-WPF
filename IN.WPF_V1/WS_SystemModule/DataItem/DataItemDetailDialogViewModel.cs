﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.Module;
using DevExpress.Mvvm.POCO;

namespace IN.DX.WS_SystemModule
{
    [POCOViewModel]
    public class DataItemDetailDialogViewModel
    {
        private readonly TB_DataItemDetailBll DataItemDetailBll = new TB_DataItemDetailBll();
        #region 属性

        //属性：数据字典
        public virtual TB_DataItemDetailEntity Entity { get; set; }

        #endregion

        #region 构造函数 无参数
        protected DataItemDetailDialogViewModel()
        {
            Entity = new TB_DataItemDetailEntity();
        }
        public static DataItemDetailDialogViewModel Create()
        {
            return ViewModelSource.Create(() => new DataItemDetailDialogViewModel());
        }
        #endregion

        #region 构造函数 一个参数
        protected DataItemDetailDialogViewModel(TB_DataItemDetailEntity entity, TB_DataItemEntity selectedEntity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity", "entity is null.");
            Load(entity, selectedEntity);
        }
        private void Load(TB_DataItemDetailEntity entity, TB_DataItemEntity selectedEntity)
        {
            this.Entity = entity;
            this.Entity.F_ItemId = selectedEntity.F_ItemId;
        }
        public static DataItemDetailDialogViewModel Create(TB_DataItemDetailEntity entity,TB_DataItemEntity selectedEntity)
        {
            return ViewModelSource.Create(() => new DataItemDetailDialogViewModel(entity, selectedEntity));
        }
        #endregion
    }
}