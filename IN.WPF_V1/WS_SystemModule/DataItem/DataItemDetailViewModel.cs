﻿using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.DX.Common.Corelib;
using IN.Module;
using System.Collections.ObjectModel;
using IN.DX.Common.Unity;
using IN.Interface;
using System.Reflection;

namespace IN.DX.WS_SystemModule
{
    [POCOViewModel]
    public class DataItemDetailViewModel :DXBaseViewModel<TB_DataItemDetailEntity>
    {
        #region 属性
        private readonly TB_DataItemDetailBll DataItemDetailBll = new TB_DataItemDetailBll();
        private readonly TB_DataItemBll DataItemBll = new TB_DataItemBll();
        
        private TB_DataItemEntity _SelectedDataItem;
        public TB_DataItemEntity SelectedDataItem 
        {
            get { return _SelectedDataItem; }
            set { _SelectedDataItem = value; RaisePropertyChanged(); }
        }


        public ObservableCollection<TB_DataItemEntity> _DataItemlList;
        public ObservableCollection<TB_DataItemEntity> DataItemList
        {
            get { return _DataItemlList; }
            set
            {
                _DataItemlList = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<TB_DataItemDetailEntity> _DataItemDetailList;
        public ObservableCollection<TB_DataItemDetailEntity> DataItemDetailList
        {
            get { return _DataItemDetailList; }
            set
            {
                _DataItemDetailList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region 构造函数
        public DataItemDetailViewModel()
        {
            DataItemList = new ObservableCollection<TB_DataItemEntity>(DataItemBll.GetListAll());

            //DataItemDetailList = new ObservableCollection<TB_DataItemDetailEntity>(DataItemDetailBll.GetListAll());

        }
        #endregion

        private DelegateCommand _SelectedChanged;
        public DelegateCommand SelectedChanged
        {
            get
            {
                if (_SelectedChanged == null)
                {
                    _SelectedChanged = new DelegateCommand(() => DataRefrashDataItemChanged(SelectedDataItem));
                }
                return _SelectedChanged;
            }
            set { _SelectedChanged = value; }
        }

        #region 用户操作 增删改查
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Add<TModel>(TModel model)
        {
            if (SelectedDataItem == null) return;
            var entity = new TB_DataItemDetailEntity();
            MessageResult result = DialogService.ShowDialog(MessageButton.OKCancel, "字典编辑", "DataItemDetailDialogView", DataItemDetailDialogViewModel.Create(entity, SelectedDataItem));
            if (result.Equals(MessageResult.OK))
            {
                if (DataItemDetailBll.Insert(entity))
                    DataRefrash(SelectedDataItem);
            }

        }

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Edit<TModel>(TModel model)
        {
            if (SelectedDataItem == null) return;
            if (model == null)
                return;
            var entity = model as TB_DataItemDetailEntity;
            MessageResult result = DialogService.ShowDialog(MessageButton.YesNo, "字典编辑", "DataItemDetailDialogView", DataItemDetailDialogViewModel.Create(entity, SelectedDataItem));
            if (result.Equals(MessageResult.Yes))
            {
                if (DataItemDetailBll.Update(entity))
                    DataRefrash(SelectedDataItem);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Del<TModel>(TModel model)
        {
            if (model == null)
                return;
            var entity = model as TB_DataItemDetailEntity;
            MessageResult result = MessageBoxService.ShowMessage("是否确认删除【" + entity.F_ItemName + "】该项?", "提醒", MessageButton.YesNo);
            if (result.Equals(MessageResult.Yes))
            {
                if (DataItemDetailBll.Delete(entity))
                    DataRefrash(SelectedDataItem);
            }

        }
        /// <summary>
        /// 字典类别
        /// </summary>
        public void DataClass()
        {
            MessageResult result = DialogService.ShowDialog(MessageButton.OK, "字典类别" ,"DataItemView", DataItemViewModel.Create());
            if (result.Equals(MessageResult.OK))
            {
                DataRefrash();
            }
        }
        #endregion

        #region 服务接口
        [ServiceProperty(SearchMode = ServiceSearchMode.PreferParents)]

        protected virtual IDialogService DialogService { get { return this.GetService<IDialogService>(); } }
        protected virtual IMessageBoxService MessageBoxService { get { return ServiceContainer.GetService<IMessageBoxService>(); } }
        protected virtual IWindowService WindowService { get {  return ServiceContainer.GetService<IWindowService>(); } }
        #endregion

        #region 刷新数据

        private void DataRefrash()
        {
            //DataItemList = null;
            DataItemList = new ObservableCollection<TB_DataItemEntity>(DataItemBll.GetListAll());
        }
        
        private void DataRefrash(TB_DataItemEntity SelectedDataItem)
        {
            if(SelectedDataItem!=null)
            {
                DataItemDetailList = null;
                DataItemDetailList = new ObservableCollection<TB_DataItemDetailEntity>(DataItemDetailBll.GetListAll().FindAll(t => t.F_ItemId.Equals(SelectedDataItem.F_ItemId)));
            }
           
        }

        private void DataRefrashDataItemChanged(TB_DataItemEntity selectedItem)
        {
            if (selectedItem !=null)
            {
                DataItemDetailList = new ObservableCollection<TB_DataItemDetailEntity>(DataItemDetailBll.GetListAll().FindAll(t => t.F_ItemId.Equals(selectedItem.F_ItemId)));
            }
            
        }
        #endregion
    }
}