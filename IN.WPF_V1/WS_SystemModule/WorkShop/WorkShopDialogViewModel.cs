﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.Module;
using DevExpress.Mvvm.POCO;
using System.Collections.ObjectModel;

namespace IN.DX.WS_SystemModule
{
    [POCOViewModel]
    public class WorkShopDialogViewModel
    {
        #region 属性

        //属性：部门
        public virtual Sys_WorkShopEntity Entity { get; set; }
        public ObservableCollection<Sys_CompanyEntity> CompanyList
        {
            get { return new ObservableCollection<Sys_CompanyEntity>(new Sys_CompanyBll().GetListAll()) ?? new ObservableCollection<Sys_CompanyEntity>(); }
        }

        #endregion

        #region 构造函数 无参数
        protected WorkShopDialogViewModel()
        {
            Entity = new Sys_WorkShopEntity();
        }
        public static WorkShopDialogViewModel Create()
        {
            return ViewModelSource.Create(() => new WorkShopDialogViewModel());
        }
        #endregion

        #region 构造函数 一个参数
        protected WorkShopDialogViewModel(Sys_WorkShopEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity", "entity is null.");
            Load(entity);
        }
        private void Load(Sys_WorkShopEntity entity)
        {
            this.Entity = entity;
        }
        public static WorkShopDialogViewModel Create(Sys_WorkShopEntity entity)
        {
            return ViewModelSource.Create(() => new WorkShopDialogViewModel(entity));
        }
        #endregion
    }
}