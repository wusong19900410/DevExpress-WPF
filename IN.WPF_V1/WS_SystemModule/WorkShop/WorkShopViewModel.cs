﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.DX.Common.Corelib;
using IN.Module;
using System.Collections.ObjectModel;

namespace IN.DX.WS_SystemModule
{
    [POCOViewModel]
    public class WorkShopViewModel: DXBaseViewModel<Sys_WorkShopEntity>
    {
        #region 属性
        private readonly Sys_WorkShopBll WorkShopBll = new Sys_WorkShopBll();

        public ObservableCollection<Sys_CompanyEntity> CompanyList
        {
            get { return new ObservableCollection<Sys_CompanyEntity>(new Sys_CompanyBll().GetListAll()) ?? new ObservableCollection<Sys_CompanyEntity>(); }
        }

        private ObservableCollection<Sys_WorkShopEntity> _WorkShopList;
        public ObservableCollection<Sys_WorkShopEntity> WorkShopList
        {
            get { return _WorkShopList; }
            set
            {
                _WorkShopList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region 构造函数
        public WorkShopViewModel()
        {

            WorkShopList = new ObservableCollection<Sys_WorkShopEntity>(WorkShopBll.GetListAll());
        }
        #endregion

        #region 用户操作 增删改查
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Add<TModel>(TModel model)
        {
            var entity = new Sys_WorkShopEntity();
            MessageResult result = DialogService.ShowDialog(MessageButton.OKCancel, "车间编辑", "WorkShopDialogView", WorkShopDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.OK))
            {
                if (WorkShopBll.Insert(entity))
                    DataRefrash();
            }

        }

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Edit<TModel>(TModel model)
        {

            if (model == null)
                return;
            var entity = model as Sys_WorkShopEntity;
            MessageResult result = DialogService.ShowDialog(MessageButton.YesNo, "车间编辑", "WorkShopDialogView", WorkShopDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.Yes))
            {
                if (WorkShopBll.Update(entity))
                    DataRefrash();
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Del<TModel>(TModel model)
        {
            MessageResult result = MessageBoxService.ShowMessage("是否确认删除该项?", "提醒", MessageButton.YesNo);
            if (result.Equals(MessageResult.Yes))
            {
                if (model == null)
                    return;
                var entity = model as Sys_WorkShopEntity;
                if (WorkShopBll.Delete(entity))
                    DataRefrash();
            }

        }
        #endregion

        #region 服务接口
        [ServiceProperty(SearchMode = ServiceSearchMode.PreferParents)]

        protected virtual IDialogService DialogService { get { return this.GetService<IDialogService>(); } }
        protected virtual IMessageBoxService MessageBoxService { get { return ServiceContainer.GetService<IMessageBoxService>(); } }
        protected virtual ICurrentWindowService CurrentWindowService { get { return null; } }
        #endregion

        #region 刷新数据
        private void DataRefrash()
        {
            WorkShopList = null;
            WorkShopList = new ObservableCollection<Sys_WorkShopEntity>(WorkShopBll.GetListAll());
        }
        #endregion
    }
}