﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using System.Collections.ObjectModel;
using IN.Module;
using DevExpress.Mvvm.POCO;

namespace IN.DX.WS_SystemModule
{
    [POCOViewModel]
    public class CompanyDialogViewModel
    {
        #region 属性

        //属性：部门
        public virtual Sys_CompanyEntity Entity { get; set; }

        #endregion

        #region 构造函数 无参数
        protected CompanyDialogViewModel()
        {
            Entity = new Sys_CompanyEntity();
        }
        public static CompanyDialogViewModel Create()
        {
            return ViewModelSource.Create(() => new CompanyDialogViewModel());
        }
        #endregion

        #region 构造函数 一个参数
        protected CompanyDialogViewModel(Sys_CompanyEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity", "entity is null.");
            Load(entity);
        }
        private void Load(Sys_CompanyEntity entity)
        {
            this.Entity = entity;
        }
        public static CompanyDialogViewModel Create(Sys_CompanyEntity entity)
        {
            return ViewModelSource.Create(() => new CompanyDialogViewModel(entity));
        }
        #endregion
    }
}