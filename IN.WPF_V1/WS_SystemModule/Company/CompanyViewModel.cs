﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.DX.Common.Corelib;
using IN.Module;
using System.Collections.ObjectModel;

namespace IN.DX.WS_SystemModule
{
    [POCOViewModel]
    public class CompanyViewModel:DXBaseViewModel<Sys_CompanyEntity>
    {
        #region 属性
        private readonly Sys_CompanyBll CompanyBll = new Sys_CompanyBll();

        private ObservableCollection<Sys_CompanyEntity> _CompanyList;
        public ObservableCollection<Sys_CompanyEntity> CompanyList
        {
            get { return _CompanyList; }
            set
            {
                _CompanyList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region 构造函数
        public CompanyViewModel()
        {
            CompanyList = new ObservableCollection<Sys_CompanyEntity>(CompanyBll.GetListAll());
        }
        #endregion

        #region 用户操作 增删改查
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Add<TModel>(TModel model)
        {
            var entity = new Sys_CompanyEntity();
            MessageResult result = DialogService.ShowDialog(MessageButton.OKCancel, "公司编辑", "CompanyDialogView", CompanyDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.OK))
            {
                if (CompanyBll.Insert(entity))
                    DataRefrash();
            }

        }

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Edit<TModel>(TModel model)
        {

            if (model == null)
                return;
            var entity = model as Sys_CompanyEntity;
            MessageResult result = DialogService.ShowDialog(MessageButton.YesNo, "公司编辑", "CompanyDialogView", CompanyDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.Yes))
            {
                if (CompanyBll.Update(entity))
                    DataRefrash();
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Del<TModel>(TModel model)
        {
            MessageResult result = MessageBoxService.ShowMessage("是否确认删除该项?", "提醒", MessageButton.YesNo);
            if (result.Equals(MessageResult.Yes))
            {
                if (model == null)
                    return;
                var entity = model as Sys_CompanyEntity;
                if (CompanyBll.Delete(entity))
                    DataRefrash();
            }

        }
        #endregion

        #region 服务接口
        [ServiceProperty(SearchMode = ServiceSearchMode.PreferParents)]

        protected virtual IDialogService DialogService { get { return this.GetService<IDialogService>(); } }
        protected virtual IMessageBoxService MessageBoxService { get { return ServiceContainer.GetService<IMessageBoxService>(); } }
        protected virtual ICurrentWindowService CurrentWindowService { get { return null; } }
        #endregion

        #region 刷新数据
        private void DataRefrash()
        {
            CompanyList = null;
            CompanyList = new ObservableCollection<Sys_CompanyEntity>(CompanyBll.GetListAll());
        }
        #endregion
    }
}