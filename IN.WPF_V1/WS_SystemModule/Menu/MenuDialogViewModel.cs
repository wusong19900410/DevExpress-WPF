﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.Module;
using DevExpress.Mvvm.POCO;
using IN.DX.WS_Home;
using System.Collections.ObjectModel;
using IN.Util;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;
using System.Globalization;
using IN.Module;
using IN.Util;

/// <summary> 
/// Industry Software
/// Copyright (c) 2020-2025  
/// 创 建：吴松
/// 描 述：菜单编辑功能
/// </summary> 
namespace IN.DX.WS_SystemModule
{
    [POCOViewModel]
    public class MenuDialogViewModel :IDataErrorInfo
    {

        private readonly Sys_MenuBll MenuBll = new Sys_MenuBll();
        private readonly Sys_ButtonBll ButtonBll = new Sys_ButtonBll();
        #region 属性
        //属性：父菜单显示需要
        public virtual ObservableCollection<TreeModel> MenuTreeList { get; set; }

        //属性：菜单项
        public virtual SYS_MenuEntity Menu { get; set; }

        //属性:按钮集合
        public virtual ObservableCollection<Sys_ButtonEntity> ButtonList { get;set;}

        //属性：按钮选定项
        public virtual Sys_ButtonEntity ButtonSelectedItem { get; set; }

        #endregion

        #region 构造函数 无参数
        protected MenuDialogViewModel() {
            ButtonSelectedItem = new Sys_ButtonEntity();
            Menu = new SYS_MenuEntity();
            MenuTreeList = new ObservableCollection<TreeModel>();
            ButtonList = new ObservableCollection<Sys_ButtonEntity>();
        }
        public static MenuDialogViewModel Create()
        {
            return ViewModelSource.Create(() => new MenuDialogViewModel());
        }
        #endregion

        #region 构造函数 一个参数(menu)
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="menu"> 菜单项 </param>
        protected MenuDialogViewModel(SYS_MenuEntity menu)
        {
            if (menu == null)
                throw new ArgumentNullException("menu", "menu is null.");
            Load(menu);
        }
        private void Load(SYS_MenuEntity menu)
        {
            this.Menu = menu;
            this.MenuTreeList =new ObservableCollection<TreeModel>( MenuBll.GetMenuCheckTree());
        }

        public static MenuDialogViewModel Create(SYS_MenuEntity menu)
        {
            return ViewModelSource.Create(() => new MenuDialogViewModel(menu));
        }
        #endregion

        #region 构造函数 二个参数(menu,buttonlist)
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="menu">菜单项</param>
        /// <param name="buttonlist">按钮集合</param>
        protected MenuDialogViewModel(SYS_MenuEntity menu , ObservableCollection<Sys_ButtonEntity> buttonlist)
        {
            if (menu == null)
                throw new ArgumentNullException("menu", "menu is null.");
            Load(menu,buttonlist);
        }
        private void Load(SYS_MenuEntity menu, ObservableCollection<Sys_ButtonEntity> buttonlist)
        {
            this.Menu = menu;
            this.MenuTreeList = new ObservableCollection<TreeModel>(MenuBll.GetMenuCheckTree());
            this.ButtonList =buttonlist;
        }

        public static MenuDialogViewModel Create(SYS_MenuEntity menu, ObservableCollection<Sys_ButtonEntity> buttonlist)
        {
            return ViewModelSource.Create(() => new MenuDialogViewModel(menu, buttonlist));
        }
        #endregion

        #region 命令
        public void ButtonAdd()
        {
            ObservableCollection<Sys_ButtonEntity> ButtonListCopy = ButtonList;
            ButtonListCopy.Add(new Sys_ButtonEntity());
            ButtonList = ButtonListCopy;
        }

        public void ButtonDelete ()
        {
            if (ButtonSelectedItem == null)
                return;
            ButtonList.Remove(ButtonSelectedItem);

        }
        #endregion

        #region 服务接口
        [ServiceProperty(SearchMode = ServiceSearchMode.PreferParents)]
        protected virtual IMessageBoxService MessageBoxService { get { return null; } }
        protected virtual ICurrentWindowService CurrentWindowService { get { return null; } }
        #endregion

        #region 输入校验
        public string Error => throw new NotImplementedException();

        public string this[string columnName] => throw new NotImplementedException();
        #endregion
    }
}