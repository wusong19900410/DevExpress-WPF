﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.Module;
using DevExpress.Mvvm.POCO;
using System.Collections.ObjectModel;
using IN.DX.Common.Corelib;
using IN.Util;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

/// <summary> 
/// Industry Software
/// Copyright (c) 2020-2025  
/// 创 建：吴松
/// 描 述：菜单功能
/// </summary> 
namespace IN.DX.WS_SystemModule
{
    [POCOViewModel]
    public class MenuViewModel : DXBaseViewModel<SYS_MenuEntity>
    {

        #region 属性
        private readonly Sys_ButtonBll ButtonBll = new Sys_ButtonBll();

        private readonly Sys_MenuBll MenuBll = new Sys_MenuBll();

        private ObservableCollection<SYS_MenuEntity> _MenuList;
        public  ObservableCollection<SYS_MenuEntity> MenuList
        {
            get { return _MenuList; }
            set 
            { 
                _MenuList = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<Sys_ButtonEntity> _ButtonList;
        public ObservableCollection<Sys_ButtonEntity> ButtonList
        {
            get { return _ButtonList; }
            set
            {
                _ButtonList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region 构造函数
        public MenuViewModel()
        {
            MenuList = new ObservableCollection<SYS_MenuEntity>(MenuBll.GetListAll());
            ButtonList = new ObservableCollection<Sys_ButtonEntity>();
        }

        public static MenuViewModel Create()
        {
            return ViewModelSource.Create(() => new MenuViewModel());
        }

        #endregion

        #region 用户操作 增删改查
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Add<TModel>(TModel model)
        {
            var menu = new SYS_MenuEntity();
            MessageResult result = DialogService.ShowDialog(MessageButton.YesNo, "Dialog", "MenuDialogView", MenuDialogViewModel.Create(menu));
            if (result.Equals(MessageResult.Yes))
            {
                if (MenuBll.Insert(menu))
                    DataRefrash();
            }

        }


        /// <summary>
        /// 修改数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Edit<TModel>(TModel model)
        {
            
            if (model == null)
                return;
            var menu = model as SYS_MenuEntity;
            var buttonList = new ObservableCollection<Sys_ButtonEntity>(ButtonBll.GetListByMenuID(menu.MenuID, "wusong"));
            MessageResult result = DialogService.ShowDialog(MessageButton.YesNo, "菜单编辑", "MenuDialogView", MenuDialogViewModel.Create(menu, buttonList));
            if(result.Equals(MessageResult.Yes))
            {
                if (MenuBll.UpdateOrAdd(menu,buttonList))
                    DataRefrash();
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Del<TModel>(TModel model)
        {
            MessageResult result = MessageBoxService.ShowMessage("是否确认删除该项?","提醒",MessageButton.YesNo);
            if (result.Equals(MessageResult.Yes))
            {
                if (model == null)
                    return;
                var menu = model as SYS_MenuEntity;
                var buttonList = new ObservableCollection<Sys_ButtonEntity>(ButtonBll.GetListByMenuID(menu.MenuID, "wusong"));
                if (MenuBll.Delete(menu, buttonList))
                    DataRefrash();
            }

        }
        #endregion

        #region 刷新数据
        private void DataRefrash()
        {
            MenuList = null;
            MenuList = new ObservableCollection<SYS_MenuEntity>(MenuBll.GetListAll());
        }
        #endregion

        #region 服务接口
        [ServiceProperty(SearchMode = ServiceSearchMode.PreferParents)]

        protected IDialogService DialogService { get { return this.GetService<IDialogService>(); } }
        protected virtual IMessageBoxService MessageBoxService { get { return ServiceContainer.GetService<IMessageBoxService>(); } }
        protected virtual ICurrentWindowService CurrentWindowService { get { return null; } }
        #endregion

    }
}