﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.DX.Common.Corelib;
using IN.Module;
using System.Collections.ObjectModel;
using IN.DX.WS_OrganizationModule;
using System.Windows;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;

namespace IN.DX.WS_OrganizationModule
{
    [POCOViewModel]
    public class RoleViewModel :DXBaseViewModel<TB_RoleEntity>
    {
        #region 属性
        private readonly TB_RoleBll RoleBll = new TB_RoleBll();

        private ObservableCollection<TB_RoleEntity> _RoleList;
        public ObservableCollection<TB_RoleEntity> RoleList
        {
            get { return _RoleList; }
            set
            {
                _RoleList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region 构造函数
        public RoleViewModel()
        {
            RoleList = new ObservableCollection<TB_RoleEntity>();
            RoleList = new ObservableCollection<TB_RoleEntity>(RoleBll.GetListAll());
        }

        #endregion

        #region 用户操作 增删改查
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Add<TModel>(TModel model)
        {
            var entity = new TB_RoleEntity();
            MessageResult result = WinUIDialogService.ShowDialog(MessageButton.OKCancel, "角色编辑", "RoleDialogView", RoleDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.OK))
            {
                if (RoleBll.Insert(entity))
                    DataRefrash();
            }

        }

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Edit<TModel>(TModel model)
        {

            if (model == null)
                return;
            var entity = model as TB_RoleEntity;
            MessageResult result = WinUIDialogService.ShowDialog(MessageButton.YesNo, "角色编辑", "RoleDialogView", RoleDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.Yes))
            {
                if (RoleBll.Update(entity))
                    DataRefrash();
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Del<TModel>(TModel model)
        {
            MessageResult result = MessageBoxService.ShowMessage("是否确认删除该项?", "提醒", MessageButton.YesNo);
            if (result.Equals(MessageResult.Yes))
            {
                if (model == null)
                    return;
                var entity = model as TB_RoleEntity;
                if (RoleBll.Delete(entity))
                    DataRefrash();
            }

        }


        public void FunctionAuth<TModel>(TModel model)
        {
            if (model == null)
                return;
            var entity = model as TB_RoleEntity;

            IDocument doc = DocumentManagerService.FindDocumentById(entity.RoleID);
            if (doc == null)
            {
                doc = DocumentManagerService.CreateDocument("AuthorizeView", AuthorizeViewModel.Create(entity));
                doc.Id = entity.RoleID;
            }
            doc.Show();
        }
        #endregion


        #region 对话框传入按钮  注释 删除 废用

        private void WorkStep(TB_RoleEntity model, int step)
        {
            UICommand result = DialogService.ShowDialog(new List<UICommand>() { BackCommand, NextCommand, FinishCommand }, "", "AuthorizeView", AuthorizeViewModel.Create(model));
            if (result.IsCancel == true)
            {
                if (result.Tag.Equals("Next"))
                {
                    WorkStep(model, step + 1);
                }
                else if (result.Tag.Equals("Back"))
                {
                    WorkStep(model, step - 1);
                }
            }

        }
        UICommand BackCommand = new UICommand()
        {
            Id = MessageBoxResult.Cancel,
            Caption = "上一步",

            Tag = "Back",
            IsCancel = true,
            IsDefault = false,
        };

        UICommand NextCommand = new UICommand()
        {
            Id = MessageBoxResult.Cancel,
            Tag="Next",
            Caption = "下一步",
            IsCancel = true,
            IsDefault = false,
        };
        UICommand FinishCommand = new UICommand()
        {
            Caption = "完成",
            IsCancel = false,
            IsDefault = true,
            Command = new DelegateCommand<CancelEventArgs>(
        x => { })
            //x => !string.IsNullOrEmpty(registrationViewModel.UserName)),
        };
        #endregion 

        #region 服务接口
        [ServiceProperty(SearchMode = ServiceSearchMode.PreferParents)]
        protected IDialogService WinUIDialogService { get { return this.GetService<IDialogService>("UIDialog"); } }
        protected IDialogService DialogService { get { return this.GetService<IDialogService>(); } }
        protected virtual IMessageBoxService MessageBoxService { get { return ServiceContainer.GetService<IMessageBoxService>(); } }
        protected virtual ICurrentWindowService CurrentWindowService { get { return null; } }
        protected IDocumentManagerService DocumentManagerService { get { return this.GetService<IDocumentManagerService>(); } }

        #endregion

        #region 刷新数据
        private void DataRefrash()
        {
            RoleList = null;
            RoleList = new ObservableCollection<TB_RoleEntity>(RoleBll.GetListAll());
        }
        #endregion
    }
}