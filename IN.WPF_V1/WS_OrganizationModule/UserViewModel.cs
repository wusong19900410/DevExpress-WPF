﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.Module;
using System.Collections.ObjectModel;
using IN.DX.Common.Corelib;
using IN.Module;

namespace IN.DX.WS_OrganizationModule
{
    [POCOViewModel]
    public class UserViewModel : DXBaseViewModel<TB_UserEntity>
    {
        #region 属性
        private readonly TB_UserBll UserBll = new TB_UserBll();

        public ObservableCollection<TB_DepartmentEntity> DepartmentList
        {
            get { return new ObservableCollection<TB_DepartmentEntity>(new TB_DepartmentBll().GetListAll()) ?? new ObservableCollection<TB_DepartmentEntity>(); }
        }

        public ObservableCollection<TB_RoleEntity> WorkShopList
        {
            get { return new ObservableCollection<TB_RoleEntity>(new TB_RoleBll().GetListAll()) ?? new ObservableCollection<TB_RoleEntity>(); }
        }

        private ObservableCollection<TB_UserEntity> _UserList;
        public ObservableCollection<TB_UserEntity> UserList
        {
            get { return _UserList; }
            set
            {
                _UserList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region 构造函数
        public UserViewModel()
        {

            UserList = new ObservableCollection<TB_UserEntity>();
            UserList = new ObservableCollection<TB_UserEntity>(UserBll.GetListAll());
        }
        #endregion

        #region 用户操作 增删改查
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Add<TModel>(TModel model)
        {
            var entity = new TB_UserEntity();
            MessageResult result = DialogService.ShowDialog(MessageButton.OKCancel, "用户编辑", "UserDialogView", UserDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.OK))
            {
                if (UserBll.Insert(entity))
                    DataRefrash();
            }

        }

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Edit<TModel>(TModel model)
        {

            if (model == null)
                return;
            var entity = model as TB_UserEntity;
            MessageResult result = DialogService.ShowDialog(MessageButton.YesNo, "用户编辑", "UserDialogView", UserDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.Yes))
            {
                if (UserBll.Update(entity))
                    DataRefrash();
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Del<TModel>(TModel model)
        {
            MessageResult result = MessageBoxService.ShowMessage("是否确认删除该项?", "提醒", MessageButton.YesNo);
            if (result.Equals(MessageResult.Yes))
            {
                if (model == null)
                    return;
                var entity = model as TB_UserEntity;
                if (UserBll.Delete(entity))
                    DataRefrash();
            }

        }
        #endregion

        #region 服务接口
        [ServiceProperty(SearchMode = ServiceSearchMode.PreferParents)]

        protected virtual IDialogService DialogService { get { return this.GetService<IDialogService>(); } }
        protected virtual IMessageBoxService MessageBoxService { get { return ServiceContainer.GetService<IMessageBoxService>(); } }
        protected virtual ICurrentWindowService CurrentWindowService { get { return null; } }
        #endregion

        #region 刷新数据
        private void DataRefrash()
        {
            UserList = null;
            UserList = new ObservableCollection<TB_UserEntity>(UserBll.GetListAll());
        }
        #endregion
    }
}