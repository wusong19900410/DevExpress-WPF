﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.Module;
using System.Collections.ObjectModel;
using DevExpress.Mvvm.POCO;

namespace IN.DX.WS_OrganizationModule
{
    [POCOViewModel]
    public class DepartmentDialogViewModel
    {
        private readonly TB_DepartmentBll DeaprtmentBll = new TB_DepartmentBll();
        #region 属性

        //属性：部门
        public virtual TB_DepartmentEntity Entity { get; set; }

        #endregion

        #region 构造函数 无参数
        protected DepartmentDialogViewModel()
        {
            Entity = new TB_DepartmentEntity();
        }
        public static DepartmentDialogViewModel Create()
        {
            return ViewModelSource.Create(() => new DepartmentDialogViewModel());
        }
        #endregion

        #region 构造函数 一个参数
        protected DepartmentDialogViewModel(TB_DepartmentEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity", "entity is null.");
            Load(entity);
        }
        private void Load(TB_DepartmentEntity entity)
        {
            this.Entity = entity;
        }
        public static DepartmentDialogViewModel Create(TB_DepartmentEntity entity)
        {
            return ViewModelSource.Create(() => new DepartmentDialogViewModel(entity));
        }
        #endregion
    }
}