﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.Module;
using DevExpress.Mvvm.POCO;

namespace IN.DX.WS_OrganizationModule
{
    [POCOViewModel]
    public class RoleDialogViewModel
    {
        private readonly TB_RoleBll RoleBll = new TB_RoleBll();
        #region 属性

        //属性：角色
        public virtual TB_RoleEntity Entity { get; set; }

        #endregion

        #region 构造函数 无参数
        protected RoleDialogViewModel()
        {
            Entity = new TB_RoleEntity();
        }
        public static RoleDialogViewModel Create()
        {
            return ViewModelSource.Create(() => new RoleDialogViewModel());
        }
        #endregion

        #region 构造函数 一个参数
        protected RoleDialogViewModel(TB_RoleEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity", "entity is null.");
            Load(entity);
        }
        private void Load(TB_RoleEntity entity)
        {
            this.Entity = entity;
        }
        public static RoleDialogViewModel Create(TB_RoleEntity entity)
        {
            return ViewModelSource.Create(() => new RoleDialogViewModel(entity));
        }
        #endregion
    }
}