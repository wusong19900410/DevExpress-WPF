﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.Module;
using System.Collections.ObjectModel;
using DevExpress.Mvvm.POCO;
using IN.Util;
using IN.Module;

namespace IN.DX.WS_OrganizationModule
{
    [POCOViewModel]
    public class AuthorizeViewModel :ViewModelBase
    {
        #region 属性
        private readonly Sys_MenuBll MenuBll = new Sys_MenuBll();
        private readonly Sys_ButtonBll ButtonBll = new Sys_ButtonBll();
        private readonly TB_AuthorizeBll AuthorizeBll = new TB_AuthorizeBll();

        private int _StepIndex;
        public int StepIndex 
        {
            get { return _StepIndex; }
            set { _StepIndex = value; RaisePropertyChanged(); } 
        }
        private TB_RoleEntity RoleEntity =new TB_RoleEntity();

        public virtual ObservableCollection<TreeModel> MenuTreeList { get; set; }
        public virtual ObservableCollection<TreeModel> ButtonTreeList { get; set; }

        public virtual ObservableCollection<TB_AuthorizeEntity> AuthorizeList { get; set;}
        #endregion

        #region 构造函数 无参数
        protected AuthorizeViewModel()
        {
            MenuTreeList = new ObservableCollection<TreeModel>(); 
            ButtonTreeList = new ObservableCollection<TreeModel>();
        }
        public static AuthorizeViewModel Create()
        {
            return ViewModelSource.Create(() => new AuthorizeViewModel());
        }
        #endregion

        #region 构造函数 一个参数(角色)
        protected AuthorizeViewModel(TB_RoleEntity entity)
        {
            RoleEntity = entity;
            MenuTreeList = new ObservableCollection<TreeModel>(MenuBll.GetMenuCheckTree());

            AuthorizeList = new ObservableCollection<TB_AuthorizeEntity>(AuthorizeBll.GetListAll().FindAll(t=>t.ObjectId.Equals(entity.RoleID)));
            TreeSet(MenuTreeList, AuthorizeList);
            ButtonTreeList = new ObservableCollection<TreeModel>(MenuBll.GetButtonCheckTree(MenuTreeList));
        }

        public static AuthorizeViewModel Create(TB_RoleEntity entity)
        {
            return ViewModelSource.Create(() => new AuthorizeViewModel(entity));
        }
        #endregion

        #region 按钮 （上一步、下一步、完成）
        //上一步
        public void GoBack()
        {
            if(StepIndex > 0) StepIndex -= 1;

        }
        public bool CanGoBack()
        {
            return !(StepIndex==0);
        } //完全是为了操作友好性
        //下一步
        public void GoNext()
        {
            if (StepIndex < 2) StepIndex += 1;
            if (StepIndex == 1)
            {
                ObservableCollection<TreeModel>  ButtonTreeListCopy = new ObservableCollection<TreeModel>(MenuBll.GetButtonCheckTree(MenuTreeList));
                TreeSet(ButtonTreeListCopy, AuthorizeList);
                ButtonTreeList = ButtonTreeListCopy;
                //根据第一步隐藏部分节点

            }
        }
        public bool CanGoNext()
        {
            return !(StepIndex == 2);
        }
        //完成
        public void GoFinish()
        {
            ObservableCollection<TB_AuthorizeEntity> MenuList = new ObservableCollection<TB_AuthorizeEntity>();
            ObservableCollection<TB_AuthorizeEntity> ButtonList = new ObservableCollection<TB_AuthorizeEntity>();
            TreeGet(MenuTreeList, MenuList);
            TreeGet2(ButtonTreeList, ButtonList);
            if (AuthorizeBll.Save(RoleEntity, MenuList,ButtonList))
            CurrentWindowService.Close();
        }
        public bool CanGoFinish()
        {
            return (StepIndex == 2);
        }
        #endregion

        #region 服务接口
        [ServiceProperty(SearchMode = ServiceSearchMode.PreferParents)]
        protected virtual IMessageBoxService MessageBoxService { get { return null; } }
        protected virtual ICurrentWindowService CurrentWindowService { get { return null; } }
        #endregion

        #region 辅助函数
        /// <summary>
        /// 用于加载树数据选中项
        /// </summary>
        /// <param name="SourceList"></param>
        /// <param name="loadList"></param>
        /// <returns></returns>
        private int TreeSet(ObservableCollection<TreeModel> SourceList, ObservableCollection<TB_AuthorizeEntity> loadList)
        {
            int nodeCount = 0;
            foreach (TreeModel tm in SourceList)
            {
                int tCount = 0; //子节点选中数量
                if (tm.ChildNodes != null && tm.ChildNodes.Count != 0)
                {
                    tCount = TreeSet(new ObservableCollection<TreeModel>(tm.ChildNodes), loadList);
                    if (tCount == tm.ChildNodes.Count) {
                        int i = 0;
                        for( ;i< tm.ChildNodes.Count;i++)
                        {
                            if(tm.ChildNodes[i].checkstate == null)
                            {
                                tm.checkstate = null;
                                break;
                            }
                        }
                        if (i == tm.ChildNodes.Count) { tm.checkstate = true; nodeCount += 1; }


                    }
                    if (tCount != tm.ChildNodes.Count && tCount > 0) 
                    { 
                        tm.checkstate = null;
                        nodeCount += 1;
                    }
                }
                else
                {
                    foreach (var m in loadList)
                    {
                        if ((m.ItemId == tm.id) || (tm.checkstate!=false))
                        {
                            nodeCount += 1;
                            tm.checkstate = true;
                            break;
                        }
                    }
                }
            }
            return nodeCount;
        }
        //获取菜单
        private void TreeGet(ObservableCollection<TreeModel> SourceList, ObservableCollection<TB_AuthorizeEntity> outputList)
        {
            foreach (TreeModel treemodel in SourceList)
            {
                if (((treemodel.checkstate == null) || (bool)treemodel.checkstate) )
                {
                    outputList.Add(new TB_AuthorizeEntity()
                    {
                        ObjectType = 1, //1角色 2用户
                        ObjectId = RoleEntity.RoleID,
                        ItemType = 1, //1菜单 2按钮
                        ItemId = treemodel.id,
                        CreateTime = DateTime.Now
                    });
                }
                if (treemodel.ChildNodes != null)
                    TreeGet(new ObservableCollection<TreeModel>(treemodel.ChildNodes), outputList);

            }
        }
        //获取按钮
        private void TreeGet2(ObservableCollection<TreeModel> SourceList, ObservableCollection<TB_AuthorizeEntity> outputList)
        {
            foreach (TreeModel treemodel in SourceList)
            {
                if ((treemodel.checkstate == null) || (bool)treemodel.checkstate)
                {
                    if(treemodel.hasChildren ==false)
                    {
                        outputList.Add(new TB_AuthorizeEntity()
                        {
                            ObjectType = 1, //1角色 2用户
                            ObjectId = RoleEntity.RoleID,
                            ItemType = 2, //1菜单 2按钮
                            ItemId = treemodel.id,
                            CreateTime = DateTime.Now
                        });
                    }
                }
                if (treemodel.ChildNodes != null)
                    TreeGet2(new ObservableCollection<TreeModel>(treemodel.ChildNodes), outputList);

            }
        }
        #endregion 
    }
}