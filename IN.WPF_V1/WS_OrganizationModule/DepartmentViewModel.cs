﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.DX.Common.Corelib;
using IN.Module;
using System.Collections.ObjectModel;

namespace IN.DX.WS_OrganizationModule
{
    [POCOViewModel]
    public class DepartmentViewModel : DXBaseViewModel<TB_DepartmentEntity>
    {
        #region 属性
        private readonly TB_DepartmentBll DepartmentBll = new TB_DepartmentBll();

        private ObservableCollection<TB_DepartmentEntity> _DepartmentList;
        public ObservableCollection<TB_DepartmentEntity> DepartmentList
        {
            get { return _DepartmentList; }
            set
            {
                _DepartmentList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region 构造函数
        public DepartmentViewModel()
        {
            DepartmentList = new ObservableCollection<TB_DepartmentEntity>();
            DepartmentList = new ObservableCollection<TB_DepartmentEntity>(DepartmentBll.GetListAll());
        }

        #endregion

        #region 用户操作 增删改查
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Add<TModel>(TModel model)
        {
            var entity = new TB_DepartmentEntity();
            MessageResult result = DialogService.ShowDialog(MessageButton.OKCancel, "部门编辑", "DepartmentDialogView", DepartmentDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.OK))
            {
                if (DepartmentBll.Insert(entity))
                    DataRefrash();
            }

        }

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Edit<TModel>(TModel model)
        {

            if (model == null)
                return;
            var entity = model as TB_DepartmentEntity;
            MessageResult result = DialogService.ShowDialog(MessageButton.YesNo, "部门编辑", "DepartmentDialogView", DepartmentDialogViewModel.Create(entity));
            if (result.Equals(MessageResult.Yes))
            {
                if (DepartmentBll.Update(entity))
                    DataRefrash();
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model"></param>
        public override void Del<TModel>(TModel model)
        {
            MessageResult result = MessageBoxService.ShowMessage("是否确认删除该项?", "提醒", MessageButton.YesNo);
            if (result.Equals(MessageResult.Yes))
            {
                if (model == null)
                    return;
                var entity = model as TB_DepartmentEntity;
                if (DepartmentBll.Delete(entity))
                    DataRefrash();
            }

        }
        #endregion

        #region 服务接口
        [ServiceProperty(SearchMode = ServiceSearchMode.PreferParents)]

        protected virtual IDialogService DialogService { get { return this.GetService<IDialogService>(); } }
        protected virtual IMessageBoxService MessageBoxService { get { return ServiceContainer.GetService<IMessageBoxService>(); } }
        protected virtual ICurrentWindowService CurrentWindowService { get { return null; } }
        #endregion

        #region 刷新数据
        private void DataRefrash()
        {
            DepartmentList = null;
            DepartmentList = new ObservableCollection<TB_DepartmentEntity>(DepartmentBll.GetListAll());
        }
        #endregion
    }
}