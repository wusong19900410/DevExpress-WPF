﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm;
using IN.Module;
using DevExpress.Mvvm.POCO;
using System.Collections.ObjectModel;

namespace IN.DX.WS_OrganizationModule
{
    [POCOViewModel]
    public class UserDialogViewModel
    {
        private readonly TB_UserBll UserBll = new TB_UserBll();
        #region 属性

        //属性：角色
        public virtual TB_UserEntity Entity { get; set; }

        public ObservableCollection<TB_DepartmentEntity> DepartmentList
        {
            get { return new ObservableCollection<TB_DepartmentEntity>(new TB_DepartmentBll().GetListAll()) ?? new ObservableCollection<TB_DepartmentEntity>(); }
        }

        public ObservableCollection<TB_RoleEntity> RoleList
        {
            get { return new ObservableCollection<TB_RoleEntity>(new TB_RoleBll().GetListAll()) ?? new ObservableCollection<TB_RoleEntity>(); }
        }

        #endregion

        #region 构造函数 无参数
        protected UserDialogViewModel()
        {
            Entity = new TB_UserEntity();
        }
        public static UserDialogViewModel Create()
        {
            return ViewModelSource.Create(() => new UserDialogViewModel());
        }
        #endregion

        #region 构造函数 一个参数
        protected UserDialogViewModel(TB_UserEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity", "entity is null.");
            Load(entity);
        }
        private void Load(TB_UserEntity entity)
        {
            this.Entity = entity;
        }
        public static UserDialogViewModel Create(TB_UserEntity entity)
        {
            return ViewModelSource.Create(() => new UserDialogViewModel(entity));
        }
        #endregion
    }
}