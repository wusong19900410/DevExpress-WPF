﻿using IN.Module;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace IN.DX
{
    public static class GlobalBLL
    {
        #region 01 系统登录用户信息
        private static TB_UserEntity sysUser = new TB_UserEntity();
        public static TB_UserEntity SysUser
        {
            get
            {
                return sysUser;
            }
            set { sysUser = value; }
        }
        #endregion

    }

    /// <summary>
    /// 程序类型
    /// </summary>
    public enum AppType
    {
        SingleClient,  /*单机程序*/
        MultipleClient /*多机程序*/
    }
}
