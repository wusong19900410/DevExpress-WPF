﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Module

{
    /// <summary> 
    /// Industry Software
    /// Copyright (c) 2019-2025  
    /// 创 建：吴松 
    /// 日 期：2020-07-01 15:27 
    /// 描 述：1 
    /// </summary> 
    [Dapper.Contrib.Extensions.Table("SYS_Menu")]
    public class SYS_MenuEntity
    {
        #region   实体成员 
        /// <summary> 
        /// MenuID 
        /// </summary> 
        /// <returns></returns> 
        
        [Column("MENUID")]
        [Dapper.Contrib.Extensions.ExplicitKey]
        public string MenuID { get; set; }
        /// <summary> 
        /// Caption 
        /// </summary> 
        /// <returns></returns> 
        [Column("CAPTION")]
        public string Caption { get; set; }

        /// <summary> 
        /// ParentMenuID 
        /// </summary> 
        /// <returns></returns> 
        [Column("PARENTMENUID")]
        public string ParentMenuID { get; set; }
        /// <summary> 
        /// WpfOrWinForm 
        /// </summary> 
        /// <returns></returns> 
        [Column("WpfOrWinForm")]
        public string WpfOrWinForm { get; set; }

        /// <summary> 
        /// FormAssembly 
        /// </summary> 
        /// <returns></returns> 
        [Column("FormAssembly")]
        public string FormAssembly { get; set; }
        /// <summary> 
        /// FormName 
        /// </summary> 
        /// <returns></returns> 
        [Column("FormName")]
        public string FormName { get; set; }
        /// <summary> 
        /// ASSEMBLY 
        /// </summary> 
        /// <returns></returns> 
        [Column("ViewModel")]
        public string ViewModel { get; set; }
        /// <summary> 
        /// ViewName 
        /// </summary> 
        /// <returns></returns> 
        [Column("ViewName")]
        public string ViewName { get; set; }
        /// <summary> 
        /// MenuOrder 
        /// </summary> 
        /// <returns></returns> 
        [Column("MenuOrder")]
        public int? MenuOrder { get; set; }
        /// <summary> 
        /// MenuLogo 
        /// </summary> 
        /// <returns></returns> 
        [Column("MenuLogo")]
        public byte[] MenuLogo { get; set; }
        /// <summary> 
        /// ShowDialog 
        /// </summary> 
        /// <returns></returns> 
        [Column("AutoShow")]
        public int? AutoShow { get; set; }
        /// <summary> 
        /// ShowStyle 
        /// </summary> 
        /// <returns></returns> 
        [Column("ShowStyle")]
        public int ShowStyle { get; set; }
        /// <summary> 
        /// Target 
        /// </summary> 
        /// <returns></returns> 
        [Column("TARGET")]
        public int Target { get; set; }
        /// <summary> 
        /// IsVisible 
        /// </summary> 
        /// <returns></returns> 
        [Column("IsVisible")]
        public int IsVisible { get; set; }
        /// <summary> 
        /// IsExpand 
        /// </summary> 
        /// <returns></returns> 
        [Column("IsExpand")]
        public int IsExpand { get; set; }
        /// <summary> 
        /// ControlText 
        /// </summary> 
        /// <returns></returns> 
        [Column("IsToolBar")]
        public int IsToolBar { get; set; }
        /// <summary> 
        /// IsDesignForm 
        /// </summary> 
        /// <returns></returns> 
        [Column("IsDesignForm")]
        public int IsDesignForm { get; set; }
        /// <summary> 
        /// CreateTime 
        /// </summary> 
        /// <returns></returns> 
        [Column("CreateTime")]
        public string CreateTime { get; set; }
        /// <summary> 
        /// AutoShow 
        /// </summary> 
        /// <returns></returns> 
        [Column("AUTOSHOW")]
        public string CreateUser { get; set; }
        #endregion
        //[NotMapped]
        //public List<SYS_MenuEntity> SYS_MenuEntities { get; set; }
        #region   扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.MenuID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now.ToString();
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.MenuID = keyValue;
        }
        #endregion
    }
} 