﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IN.Util;

namespace IN.Module
{
    /// <summary>
    /// Industry Software
    /// Copyright (c) 2020-2022
    /// 创建人：吴松
    /// 日 期：2020.07.01
    /// 描 述：菜单功能 接口
    /// </summary>
    public interface Sys_MenuIBll
    {

        #region 获取数据
        /// <summary>
        /// 获取所有菜单列表
        /// </summary>
        /// <returns></returns>
        List<SYS_MenuEntity> GetListAll();

        /// <summary>
        /// 获取所有菜单列表 - 树结构
        /// </summary>
        /// <returns></returns>
        List<TreeModel> GetTreeListAll();
        #endregion


        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <returns></returns>
        bool Delete(string KeyValue);


        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <param name="menuEntity"></param>
        void SaveEntity(string KeyValue, SYS_MenuEntity menuEntity);

        #endregion

    }
}
