﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Module
{
    /// <summary> 
    /// Industry Software
    /// Copyright (c) 2019-2025 
    /// 创 建：吴松 
    /// 日 期：2020-07-11 20:16 
    /// 描 述：1 
    /// </summary> 
    [Dapper.Contrib.Extensions.Table("SYS_Button")]
    public class Sys_ButtonEntity
    {
        #region   实体成员 
        /// <summary> 
        /// ButtonID 
        /// </summary> 
        /// <returns></returns> 

        [Column("BUTTONID")]
        [Dapper.Contrib.Extensions.ExplicitKey]
        public string ButtonID { get; set; }
        /// <summary> 
        /// ButtonMenuID 
        /// </summary> 
        /// <returns></returns> 
        [Column("BUTTONMENUID")]
        public string ButtonMenuID { get; set; }
        /// <summary> 
        /// ButtonName 
        /// </summary> 
        /// <returns></returns> 
        [Column("BUTTONNAME")]
        public string ButtonName { get; set; }
        /// <summary> 
        /// ButtonIcon 
        /// </summary> 
        /// <returns></returns> 
        [Column("BUTTONICON")]
        public byte[] ButtonIcon { get; set; }
        /// <summary> 
        /// ButtonCommand 
        /// </summary> 
        /// <returns></returns> 
        [Column("BUTTONCOMMAND")]
        public string ButtonCommand { get; set; }
        /// <summary> 
        /// ButtonSort 
        /// </summary> 
        /// <returns></returns> 
        [Column("BUTTONSORT")]
        public int? ButtonSort { get; set; }
        /// <summary> 
        /// ButtonIsVisiable 
        /// </summary> 
        /// <returns></returns> 
        [Column("ENABLEDMARK")]
        public int? EnabledMark { get; set; }
        /// <summary> 
        /// CreateTime 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATETIME")]
        public DateTime? CreateTime { get; set; }
        /// <summary> 
        /// MenuOrder 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATEUSER")]
        public string CreateUser { get; set; }
 
        #endregion

        #region   扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.ButtonID = Guid.NewGuid().ToString();
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.ButtonID = keyValue;
        }
        #endregion
    }
}
