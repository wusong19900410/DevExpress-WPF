﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IN.DataBase;
using IN.Util;

namespace IN.Module
{
    /// <summary>
    /// Industry Software
    /// Copyright (c) 2020-2022
    /// 创建人：吴松
    /// 日 期：2020.07.01
    /// 描 述：菜单功能 BLL
    /// </summary>
    public class Sys_ButtonBll : BaseBLL<Sys_ButtonEntity>
    {
        private readonly TB_AuthorizeBll AuthorizeBll = new TB_AuthorizeBll();
        #region 获取数据
        /// <summary>
        /// 获取所有按钮列表数据
        /// </summary>
        /// <returns></returns>
        public new List<Sys_ButtonEntity> GetListAll()
        {
            List<Sys_ButtonEntity> Entity = base.GetListAll().OrderBy(t => t.ButtonSort).ToList();
            return Entity;
        }

        /// <summary>
        /// 根据按钮所属菜单，获取按钮列表数据
        /// </summary>
        /// <returns></returns>
        public  List<Sys_ButtonEntity> GetListByMenuID(string MenuID)
        {
            List<Sys_ButtonEntity> Entity = base.GetListAll().Where(t=>t.ButtonMenuID.Equals(MenuID)).OrderBy(t => t.ButtonSort).ToList();
            return Entity;
        }

        /// <summary>
        /// 根据按钮所属菜单和用户权限，获取按钮列表数据
        /// </summary>
        /// <param name="MenuID"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public List<Sys_ButtonEntity> GetListByMenuID(string MenuID,string RoleID)
        {
            List<Sys_ButtonEntity> Entity;
            //List<TB_AuthorizeEntity> authList = AuthorizeBll.GetListAll().FindAll(t => t.ObjectId.Equals(RoleID) && t.ItemType == 2 && t.ObjectType == 1);
            List<TB_AuthorizeEntity> authList = AuthorizeBll.GetListAll().FindAll(t =>  t.ItemType == 2 && t.ObjectType == 1);
            if (RoleID == "wusong")
            {
                 Entity = base.GetListAll().Where(t => t.ButtonMenuID.Equals(MenuID)).OrderBy(t => t.ButtonSort).ToList();
            }
            else
            {
                 Entity = base.GetListAll().Where(t => authList.Select(s => s.ItemId).Contains(t.ButtonID)  && t.ButtonMenuID.Equals(MenuID) && t.EnabledMark==1).OrderBy(t => t.ButtonSort).ToList();
            }
            
            return Entity;
        }
        #endregion


        #region 提交数据
        /// <summary>
        /// 删除数据  功能待开发
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <returns></returns>
        public bool Delete(string KeyValue)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 保存数据   功能待开发
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <param name="menuEntity"></param>
        public void SaveEntity(string KeyValue, SYS_MenuEntity menuEntity)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
