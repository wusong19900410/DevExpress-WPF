﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Module
{
    [Dapper.Contrib.Extensions.Table("SYS_Company")]
    public class Sys_CompanyEntity
    {
        #region   实体成员 
        /// <summary> 
        /// MenuID 
        /// </summary> 
        /// <returns></returns> 

        [Column("CompanyID")]
        [Dapper.Contrib.Extensions.ExplicitKey]
        public string CompanyID { get; set; }
        /// <summary> 
        /// Caption 
        /// </summary> 
        /// <returns></returns> 
        [Column("CompanyName")]
        public string CompanyName { get; set; }
        /// <summary> 
        /// ControlType 
        /// </summary> 
        /// <returns></returns> 
        [Column("CompanyCode")]
        public string CompanyCode { get; set; }
        /// <summary> 
        /// ParentMenuID 
        /// </summary> 
        /// <returns></returns> 
        [Column("CompanyJaneSpell")]
        public string CompanyJaneSpell { get; set; }
        /// <summary> 
        /// ASSEMBLY 
        /// </summary> 
        /// <returns></returns> 
        [Column("CompanySite")]
        public string CompanySite { get; set; }
        /// <summary> 
        /// NameSpaceAndClass 
        /// </summary> 
        /// <returns></returns> 
        [Column("CompanyEmail")]
        public string CompanyEmail { get; set; }
        /// <summary> 
        /// ShowDialog 
        /// </summary> 
        /// <returns></returns> 
        [Column("CompanyPhone")]
        public string CompanyPhone { get; set; }
        /// <summary> 
        /// ContainerForm 
        /// </summary> 
        /// <returns></returns> 
        [Column("CreateUser")]
        public string CreateUser { get; set; }
        public DateTime? CreateTime { get; set; }
        [Column("ModifyUser")]
        public string ModifyUser { get; set; }
        public DateTime? ModifyTime { get; set; }
        /// <summary> 
        /// MenuLogo 
        /// </summary> 
        /// <returns></returns> 
        [Column("CompanyLOGO")]
        public byte[] CompanyLOGO { get; set; }

        #endregion

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.CompanyID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify()
        {
            this.ModifyTime = DateTime.Now;
        }
    }
}
