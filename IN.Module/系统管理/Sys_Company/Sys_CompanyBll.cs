﻿using IN.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IN.Module
{
    public class Sys_CompanyBll : BaseBLL<Sys_CompanyEntity>, Sys_CompanyIBll
    {
        public new List<Sys_CompanyEntity> GetListAll()
        {
            List<Sys_CompanyEntity> companyEntity = base.GetListAll().OrderBy(t => t.CompanyName).ToList();
            return companyEntity;
        }

        public bool Delete(string KeyValue)
        {
            throw new NotImplementedException();
        }

        public void SaveEntity(string KeyValue, Sys_CompanyEntity factoryEntity)
        {
            throw new NotImplementedException();
        }
    }
}
