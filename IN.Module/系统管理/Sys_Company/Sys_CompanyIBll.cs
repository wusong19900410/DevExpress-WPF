﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Module
{
    public interface Sys_CompanyIBll
    {
        List<Sys_CompanyEntity> GetListAll();
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <returns></returns>
        bool Delete(string KeyValue);


        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <param name="factoryEntity"></param>
        void SaveEntity(string KeyValue, Sys_CompanyEntity factoryEntity);
    }
}
