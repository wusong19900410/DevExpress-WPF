﻿using Dapper;
using IN.DataBase;
using IN.Module.Sys_Device;
using IN.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace IN.Module
{
    public class Sys_WorkShopBll : BaseBLL<Sys_WorkShopEntity>
    {
        private readonly BaseService<Sys_WorkShopEntity> baseService = new BaseService<Sys_WorkShopEntity>();

        public new List<Sys_WorkShopEntity> GetListAll()
        {
            List<Sys_WorkShopEntity> workEntity = base.GetListAll().OrderBy(t => t.WorkShopID).ToList();
            return workEntity;
        }

        //Li.Hui
        public List<Sys_WorkShopEntity> GetWorkShopList()
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("SELECT a.WorkshopID,a.CompanyID,a.WorkShopName,a.WorkshopCode,a.WorkshopDesc,a.CreateUser,a.CreateTime,b.FactoryName ");
            strSql.Append("FROM SYS_WorkShop a,SYS_Factory b ");
            strSql.Append("where a.FactoryID=b.FactoryID");
            List<Sys_WorkShopEntity> workShopEntityList;
            using (IDbConnection dbConnection = baseService.Connection)
            {
                workShopEntityList = dbConnection.Query<Sys_WorkShopEntity>(strSql.ToString()).ToList();
            }
            return workShopEntityList;
        }

        /// <summary>
        /// 删除数据  功能待开发
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <returns></returns>
        public bool Delete(string KeyValue)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <param name="workEntity"></param>
        public void SaveEntity(string KeyValue, Sys_WorkShopEntity workEntity)
        {
            throw new NotImplementedException();
        }

    }
}
