﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Module
{
    [Dapper.Contrib.Extensions.Table("SYS_WorkShop")]
    public class Sys_WorkShopEntity
    {
        /// <summary>
        /// WorkshopID
        /// </summary>
        [Column("WORKSHOPID")]
        [Dapper.Contrib.Extensions.ExplicitKey]
        public string WorkShopID { get; set; }

        /// <summary>
        /// FactoryID
        /// </summary>
        [Column("COMPANYYID")]

        public string CompanyID { get; set; }

        ///<summary>
        ///ShopName
        /// </summary>
        [Column("WORKSHOPNAME")]
        public string WorkShopName { get; set; }
        ///<summary>
        ///WorkshopCode
        /// </summary>
        [Column("WORKSHOPCODE")]
        public string WorkShopCode { get; set; }

        ///<summary>
        ///WorkshopJaneSpell
        /// </summary>
        [Column("WORKSHOPDesc")]
        public string WorkShopDesc { get; set; }

        ///<summary>
        ///Enabled
        /// </summary>
        [Column("CREATEUSER")]
        public string CreateUser { get; set; }
        ///<summary>
        ///SaveTime
        /// </summary>
        [Column("CreateTIME")]
        public DateTime? CreateTime { get; set; }

        ///<summary>
        ///SaveTime
        /// </summary>
        [Column("ModifyTIME")]
        public DateTime? ModifyTime { get; set; }

        #region   扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.WorkShopID = Guid.NewGuid().ToString();
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.WorkShopID = keyValue;
        }
        #endregion
    }
}
