﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Module
{
    /// <summary> 
    /// Industry Software
    /// Copyright (c) 2019-2025  
    /// 创 建：吴松 
    /// 日 期：2020-07-13 12:37 
    /// 描 述：1 
    /// </summary> 
    [Dapper.Contrib.Extensions.Table("TB_Authorize")]
    public class TB_AuthorizeEntity
    {
        #region 吴松 实体成员 
        /// <summary> 
        /// 授权菜单按钮主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("AUTHORIZEID")]
        [Dapper.Contrib.Extensions.ExplicitKey]
        public string AuthorizeId { get; set; }
        /// <summary> 
        ///  对象分类:1-角色2-用户 
        /// </summary> 
        /// <returns></returns> 
        [Column("OBJECTTYPE")]
        public int? ObjectType { get; set; }
        /// <summary> 
        /// 对象主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("OBJECTID")]
        public string ObjectId { get; set; }
        /// <summary> 
        /// 项目类型:1-菜单2-按钮3-视图 
        /// </summary> 
        /// <returns></returns> 
        [Column("ITEMTYPE")]
        public int? ItemType { get; set; }
        /// <summary> 
        /// 项目主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("ITEMID")]
        public string ItemId { get; set; }
        /// <summary> 
        /// CreateUser 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATEUSER")]
        public string CreateUser { get; set; }
        /// <summary> 
        /// 创建时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATETIME")]
        public DateTime? CreateTime { get; set; }
        #endregion

        #region 吴松 扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.AuthorizeId = Guid.NewGuid().ToString();
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.AuthorizeId = keyValue;
        }
        #endregion
    }
}
