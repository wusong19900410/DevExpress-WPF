﻿using IN.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IN.Module
{
    /// <summary>
    /// Industry Software
    /// Copyright (c) 2020-2022
    /// 创建人：吴松
    /// 日 期：2020.07.03
    /// 描 述：报警信息 BLL
    /// </summary>
    public class TB_UserBll : BaseBLL<TB_UserEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取所有用户列表
        /// </summary>
        /// <returns></returns>
        public new List<TB_UserEntity> GetListAll()
        {
            List<TB_UserEntity> listEntity = base.GetListAll().OrderBy(t => t.UserCode).ToList();
            return listEntity;
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据  
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <returns></returns>
        public bool Delete(string KeyValue)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns></returns>
        public new bool Insert(TB_UserEntity entity)
        {
            bool res = false;
            try
            {
                if (string.IsNullOrEmpty(entity.UserID))
                {
                    entity.Create();
                    base.Insert(entity);
                    res = true;
                }
                else
                {
                    res = base.Update(entity);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return res;
        }


        public new bool Update(TB_UserEntity entity)
        {
            bool res = false;
            try
            {
                if (!string.IsNullOrEmpty(entity.UserID))
                {
                    entity.Modify();
                    base.Update(entity);
                    res = true;
                }
                else
                {
                    res = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return res;
        }

        #endregion
    }
}
