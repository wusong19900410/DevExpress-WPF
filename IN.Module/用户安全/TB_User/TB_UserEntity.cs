﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Module
{
    /// <summary> 
    /// Industry Software
    /// Copyright (c) 2019-2025  
    /// 创 建：吴松 
    /// 日 期：2020-07-13 16:56 
    /// 描 述：1 
    /// </summary> 
    [Dapper.Contrib.Extensions.Table("TB_User")]
    public class TB_UserEntity
    {
        #region 吴松 实体成员 
        /// <summary> 
        /// UserID 
        /// </summary> 
        /// <returns></returns> 
        [Column("USERID")]
        [Dapper.Contrib.Extensions.ExplicitKey]
        public string UserID { get; set; }
        /// <summary> 
        /// DepartmentID 
        /// </summary> 
        /// <returns></returns> 
        [Column("DEPARTMENTID")]
        public string DepartmentID { get; set; }
        /// <summary> 
        /// RoleID 
        /// </summary> 
        /// <returns></returns> 
        [Column("ROLEID")]
        public string RoleID { get; set; }
        /// <summary> 
        /// UserCode 
        /// </summary> 
        /// <returns></returns> 
        [Column("USERCODE")]
        public string UserCode { get; set; }
        /// <summary> 
        /// UserPassword 
        /// </summary> 
        /// <returns></returns> 
        [Column("USERPASSWORD")]
        public string UserPassword { get; set; }
        /// <summary> 
        /// RealName 
        /// </summary> 
        /// <returns></returns> 
        [Column("REALNAME")]
        public string RealName { get; set; }
        /// <summary> 
        /// Email 
        /// </summary> 
        /// <returns></returns> 
        [Column("EMAIL")]
        public string Email { get; set; }
        /// <summary> 
        /// Phone 
        /// </summary> 
        /// <returns></returns> 
        [Column("PHONE")]
        public string Phone { get; set; }
        /// <summary> 
        /// Jobs 
        /// </summary> 
        /// <returns></returns> 
        [Column("JOBS")]
        public string Jobs { get; set; }

        /// <summary> 
        /// DeleteMark 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISENABLEMARK")]
        public string IsEnableMark { get; set; }
        /// <summary> 
        /// DeleteMark 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISDELETEMARK")]
        public string IsDeleteMark { get; set; }
        /// <summary> 
        /// CreateUser 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATEUSER")]
        public string CreateUser { get; set; }
        /// <summary> 
        /// SaveTime 
        /// </summary> 
        /// <returns></returns> 
        [Column("CRESTETIME")]
        public DateTime? CreateTime { get; set; }
        /// <summary> 
        /// CreateUser 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFYUSER")]
        public string ModifyUser { get; set; }
        /// <summary> 
        /// SaveTime 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFYTIME")]
        public DateTime? ModifyTime { get; set; }
        #endregion

        #region 吴松 扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.UserID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify()
        {
            this.ModifyTime = DateTime.Now;
        }
        #endregion
    }
}
