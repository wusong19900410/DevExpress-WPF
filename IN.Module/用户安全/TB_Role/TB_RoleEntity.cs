﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace IN.Module
{
    /// <summary> 
    /// Industry Software
    /// Copyright (c) 2020-2025  
    /// 创 建：吴松 
    /// 日 期：2020-08-01 15:27 
    /// 描 述：角色
    /// </summary> 
    [Dapper.Contrib.Extensions.Table("TB_Role")]
    public class TB_RoleEntity
    {
        #region   实体成员 
        /// <summary> 
        /// RoleID 
        /// </summary> 
        /// <returns></returns> 
        [Column("ROLEID")]
        [Dapper.Contrib.Extensions.ExplicitKey]
        public string RoleID { get; set; }
        /// <summary> 
        /// RoleName 
        /// </summary> 
        /// <returns></returns> 
        [Column("ROLENAME")]
        public string RoleName { get; set; }
        /// <summary> 
        /// RoleRemark 
        /// </summary> 
        /// <returns></returns> 
        [Column("ROLEREMARK")]
        public string RoleRemark { get; set; }
        /// <summary> 
        /// IsDeleteMark 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISDELETEMARK")]
        public int? IsDeleteMark { get; set; }

        /// <summary> 
        /// IsEnableMark 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISENABLEMARK")]
        public int? IsEnableMark { get; set; }

        /// <summary> 
        /// CreateUser 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary> 
        /// CreateTime 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATETIME")]
        public DateTime? CreateTime { get; set; }
        /// <summary> 
        /// ModifyUser 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFYUSER")]
        public string ModifyUser { get; set; }
        /// <summary> 
        /// ModifyTime 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFYTIME")]
        public DateTime? ModifyTime { get; set; }
        
        #endregion

        #region   扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.RoleID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify()
        {
            this.ModifyTime = DateTime.Now;
        }
        #endregion
    }
}
