﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IN.DataBase;

namespace IN.Module
{
    /// <summary> 
    /// Industry Software
    /// Copyright (c) 2020-2025  
    /// 创 建：吴松 
    /// 日 期：2020-08-02 15:27 
    /// 描 述：角色
    /// </summary> 
    public class TB_RoleBll:BaseBLL<TB_RoleEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取所有角色列表数据
        /// </summary>
        /// <returns></returns>
        public new List<TB_RoleEntity> GetListAll()
        {
            List<TB_RoleEntity> Entity = base.GetListAll().OrderBy(t => t.RoleName).ToList();
            return Entity;
        }

        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据  
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <returns></returns>
        public bool Delete(string KeyValue)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns></returns>
        public new bool Insert(TB_RoleEntity entity)
        {
            bool res = false;
            try
            {
                if (string.IsNullOrEmpty(entity.RoleID))
                {
                    entity.Create();
                    base.Insert(entity);
                    res = true;
                }
                else
                {
                    res = base.Update(entity);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return res;
        }


        public new bool Update(TB_RoleEntity entity)
        {
            bool res = false;
            try
            {
                if (!string.IsNullOrEmpty(entity.RoleID))
                {
                    entity.Modify();
                    base.Update(entity);
                    res = true;
                }
                else
                {
                    res = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return res;
        }

        #endregion
    }
}
