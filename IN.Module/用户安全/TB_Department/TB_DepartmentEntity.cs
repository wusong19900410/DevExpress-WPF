﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Module
{
    /// <summary> 
    /// Industry Software
    /// Copyright (c) 2019-2025  
    /// 创 建：吴松 
    /// 日 期：2020-07-01 15:27 
    /// 描 述：部门信息
    /// </summary> 
    [Dapper.Contrib.Extensions.Table("TB_Department")]
    public class TB_DepartmentEntity
    {
        #region   实体成员 
        /// <summary> 
        /// DepartmentID 
        /// </summary> 
        /// <returns></returns> 

        [Column("DEPARTMENTID")]
        [Dapper.Contrib.Extensions.ExplicitKey]
        public string DepartmentID { get; set; }
        /// <summary> 
        /// CompanyID 
        /// </summary> 
        /// <returns></returns> 
        [Column("COMPANYID")]
        public string CompanyID { get; set; }
        /// <summary> 
        /// DepartmentName 
        /// </summary> 
        /// <returns></returns> 
        [Column("DEPARTMENTNAME")]
        public string DepartmentName { get; set; }

        /// <summary> 
        /// DepartmentCode 
        /// </summary> 
        /// <returns></returns> 
        [Column("DEPARTMENTCODE")]
        public string DepartmentCode { get; set; }
        /// <summary> 
        /// DepartmentRemark 
        /// </summary> 
        /// <returns></returns> 
        [Column("DEPARTMENTREMARK")]
        public string DepartmentRemark { get; set; }
        /// <summary> 
        /// ParentID 
        /// </summary> 
        /// <returns></returns> 
        [Column("PARENTID")]
        public string ParentID { get; set; }
        /// <summary> 
        /// IsDeleteMark 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISDELETEMARK")]
        public int? IsDeleteMark { get; set; }
        /// <summary> 
        /// IsEnableMark 
        /// </summary> 
        /// <returns></returns> 
        [Column("ISENABLEMARK")]
        public int? IsEnableMark { get; set; }
        /// <summary> 
        /// CreateUser 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATEUSER")]
        public string CreateUser { get; set; }
        /// <summary> 
        /// CreateTime 
        /// </summary> 
        /// <returns></returns> 
        [Column("CreateTime")]
        public DateTime? CreateTime { get; set; }
        /// <summary> 
        /// ModifyUser 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFYUSER")]
        public string ModifyUser { get; set; }
        /// <summary> 
        /// ModifyTime 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFYTIME")]
        public DateTime? ModifyTime { get; set; }

        #endregion

        #region   扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.DepartmentID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        public void Modify()
        {
            this.ModifyTime = DateTime.Now;
        }
        #endregion
    }
}
