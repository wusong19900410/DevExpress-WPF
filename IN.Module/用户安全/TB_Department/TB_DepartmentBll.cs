﻿using IN.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IN.Module
{
    /// <summary>
    /// Industry Software
    /// Copyright (c) 2020-2025
    /// 创建人：吴松
    /// 日 期：2020.07.01
    /// 描 述：部门信息
    /// </summary>
    public class TB_DepartmentBll : BaseBLL<TB_DepartmentEntity>
    {

        #region 获取数据
        /// <summary>
        /// 获取所有部门列表数据
        /// </summary>
        /// <returns></returns>
        public new List<TB_DepartmentEntity> GetListAll()
        {
            List<TB_DepartmentEntity> Entity = base.GetListAll().OrderBy(t => t.DepartmentName).ToList();
            return Entity;
        }

        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据  
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <returns></returns>
        public bool Delete(string KeyValue)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns></returns>
        public new bool Insert(TB_DepartmentEntity entity)
        {
            bool res = false;
            try
            {
                if (string.IsNullOrEmpty(entity.DepartmentID))
                {
                    entity.Create();
                    base.Insert(entity);
                    res = true;
                }
                else
                {
                    res = base.Update(entity);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return res;
        }


        public new bool Update(TB_DepartmentEntity entity)
        {
            bool res = false;
            try
            {
                if (!string.IsNullOrEmpty(entity.DepartmentID))
                {
                    entity.Modify();
                    base.Update(entity);
                    res = true;
                }
                else
                {
                    res =false;
                }
            }
            catch (Exception ex)
            {
            MessageBox.Show(ex.Message);
                }
            return res;
        }

#endregion
    }
}
