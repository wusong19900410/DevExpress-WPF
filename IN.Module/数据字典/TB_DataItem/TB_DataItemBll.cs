﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IN.DataBase;

namespace IN.Module
{
    public class TB_DataItemBll :BaseBLL<TB_DataItemEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取所有部门列表数据
        /// </summary>
        /// <returns></returns>
        public new List<TB_DataItemEntity> GetListAll()
        {
            List<TB_DataItemEntity> Entity = base.GetListAll().OrderBy(t => t.F_SortCode).ToList();
            return Entity;
        }

        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据  
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <returns></returns>
        public bool Delete(string KeyValue)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns></returns>
        public new bool Insert(TB_DataItemEntity entity)
        {
            bool res = false;
            try
            {
                if (string.IsNullOrEmpty(entity.F_ItemId))
                {
                    entity.Create();
                    base.Insert(entity);
                    res = true;
                }
                else
                {
                    res = base.Update(entity);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return res;
        }


        public new bool Update(TB_DataItemEntity entity)
        {
            bool res = false;
            try
            {
                if (!string.IsNullOrEmpty(entity.F_ItemId))
                {
                    entity.Modify();
                    base.Update(entity);
                    res = true;
                }
                else
                {
                    res = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return res;
        }

        #endregion
    }
}
