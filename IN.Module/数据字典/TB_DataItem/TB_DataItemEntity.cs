﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Module
{
    /// <summary> 
    /// 版 本 NEWU 3.0 北京万向 http://www.newu.com/ 
    /// Copyright (c) 2019-2025  
    /// 创 建：吴松 
    /// 日 期：2020-08-06 08:37 
    /// 描 述：数据字典
    /// </summary> 
    [Dapper.Contrib.Extensions.Table("TB_DataItem")]
    public class TB_DataItemEntity
    {
        #region   实体成员 
        /// <summary> 
        /// F_ItemId 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ITEMID")]
        [Dapper.Contrib.Extensions.ExplicitKey]
        public string F_ItemId { get; set; }
        /// <summary> 
        /// F_ParentId 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_PARENTID")]
        public string F_ParentId { get; set; }
        /// <summary> 
        /// F_ItemCode 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ITEMCODE")]
        public string F_ItemCode { get; set; }
        /// <summary> 
        /// F_ItemName 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ITEMNAME")]
        public string F_ItemName { get; set; }
        /// <summary> 
        /// F_IsTree 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ISTREE")]
        public int? F_IsTree { get; set; }
        /// <summary> 
        /// F_IsNav 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ISNAV")]
        public int? F_IsNav { get; set; }
        /// <summary> 
        /// F_SortCode 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_SORTCODE")]
        public int? F_SortCode { get; set; }
        /// <summary> 
        /// F_DeleteMark 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_DELETEMARK")]
        public int? F_DeleteMark { get; set; }
        /// <summary> 
        /// F_EnabledMark 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ENABLEDMARK")]
        public int? F_EnabledMark { get; set; }
        /// <summary> 
        /// F_Description 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_DESCRIPTION")]
        public string F_Description { get; set; }
        /// <summary> 
        /// CreateUser 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATEUSER")]
        public string CreateUser { get; set; }
        /// <summary> 
        /// CreateTime 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATETIME")]
        public DateTime? CreateTime { get; set; }
        /// <summary> 
        /// ModifyUser 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFYUSER")]
        public string ModifyUser { get; set; }
        /// <summary> 
        /// ModifyTime 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFYTIME")]
        public DateTime? ModifyTime { get; set; }
        #endregion

        #region   扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.F_ItemId = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify()
        {
            this.ModifyTime = DateTime.Now;
        }
        #endregion
    }
}
