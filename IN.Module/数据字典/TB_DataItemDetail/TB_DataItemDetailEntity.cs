﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN.Module
{
    /// <summary> 
    /// Industry Software
    /// Copyright (c) 2019-2025  
    /// 创 建：吴松 
    /// 日 期：2020-08-06 08:41 
    /// 描 述：数据字典 
    /// </summary> 
    [Dapper.Contrib.Extensions.Table("TB_DataItemDetail")]
    public class TB_DataItemDetailEntity
    {
        #region   实体成员 
        /// <summary> 
        /// F_ItemDetailId 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ITEMDETAILID")]
        [Dapper.Contrib.Extensions.ExplicitKey]
        public string F_ItemDetailId { get; set; }
        /// <summary> 
        /// F_ItemId 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ITEMID")]
        public string F_ItemId { get; set; }
        /// <summary> 
        /// F_ParentId 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_PARENTID")]
        public string F_ParentId { get; set; }
        /// <summary> 
        /// F_ItemCode 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ITEMCODE")]
        public string F_ItemCode { get; set; }
        /// <summary> 
        /// F_ItemName 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ITEMNAME")]
        public string F_ItemName { get; set; }
        /// <summary> 
        /// F_ItemValue 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ITEMVALUE")]
        public string F_ItemValue { get; set; }
        /// <summary> 
        /// F_QuickQuery 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_QUICKQUERY")]
        public string F_QuickQuery { get; set; }
        /// <summary> 
        /// F_IsDefault 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ISDEFAULT")]
        public int? F_IsDefault { get; set; }
        /// <summary> 
        /// F_SortCode 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_SORTCODE")]
        public int? F_SortCode { get; set; }
        /// <summary> 
        /// F_DeleteMark 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_DELETEMARK")]
        public int? F_DeleteMark { get; set; }
        /// <summary> 
        /// F_EnabledMark 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_ENABLEDMARK")]
        public int? F_EnabledMark { get; set; }
        /// <summary> 
        /// 备注 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_DESCRIPTION")]
        public string F_Description { get; set; }
        /// <summary> 
        /// 创建用户 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATEUSER")]
        public string CreateUser { get; set; }
        /// <summary> 
        /// 创建日期 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATETIME")]
        public DateTime? CreateTime { get; set; }
        /// <summary> 
        /// 修改用户主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFYUSER")]
        public string ModifyUser { get; set; }
        /// <summary> 
        /// 修改日期 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFYTIME")]
        public DateTime? ModifyTime { get; set; }
        #endregion

        #region   扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.F_ItemDetailId = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify()
        {
            this.ModifyTime = DateTime.Now;
        }
        #endregion
    }
}
