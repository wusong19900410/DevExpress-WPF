﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IN.DataBase;

namespace IN.Module
{
    public class TB_DataItemDetailBll:BaseBLL<TB_DataItemDetailEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取所有字典明细列表数据
        /// </summary>
        /// <returns></returns>
        public new List<TB_DataItemDetailEntity> GetListAll()
        {
            List<TB_DataItemDetailEntity> Entity = base.GetListAll().OrderBy(t => t.F_SortCode).ToList();
            return Entity;
        }

        /// <summary>
        /// 获取字典明细列表数据
        /// </summary>
        /// <returns></returns>
        public  List<TB_DataItemDetailEntity> GetListAllByItemCode(string ItemCode)
        {
            string sql = "SELECT a.* FROM [TB_DataItemDetail] a inner join TB_DataItem b on a.F_ItemId = b.F_ItemId and b.F_ItemCode = '" + ItemCode + "' and a.F_EnabledMark=1 Order by a.F_SortCode";
            List<TB_DataItemDetailEntity> Entity = base.iBaseService.GetBySQL(sql).ToList();
            return Entity;
        }

        /// <summary>
        /// 根据ItemName获取对应ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TB_DataItemDetailEntity GetEntityByataItemName(string id)
        {
            TB_DataItemDetailEntity Entity = base.GetListAll().FindAll(a => a.F_ItemCode.Equals(id)).FirstOrDefault();
            return Entity;
        }

        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据  
        /// </summary>
        /// <param name="KeyValue"></param>
        /// <returns></returns>
        public bool Delete(string KeyValue)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="Entity"></param>
        /// <returns></returns>
        public new bool Insert(TB_DataItemDetailEntity entity)
        {
            bool res = false;
            try
            {
                if (string.IsNullOrEmpty(entity.F_ItemDetailId))
                {
                    entity.Create();
                    base.Insert(entity);
                    res = true;
                }
                else
                {
                    res = base.Update(entity);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return res;
        }


        public new bool Update(TB_DataItemDetailEntity entity)
        {
            bool res = false;
            try
            {
                if (!string.IsNullOrEmpty(entity.F_ItemDetailId))
                {
                    entity.Modify();
                    base.Update(entity);
                    res = true;
                }
                else
                {
                    res = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return res;
        }
        #endregion
    }
}
